import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { hash, compare } from 'bcrypt';
import { UserDto } from 'src/users/dto/user.dto';
import { BagService } from 'src/bag/bag.service';
import { TokenService } from 'src/token/token.service';
import { SendUserDto } from 'src/users/dto/sendUser.dto';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private tokenService: TokenService,
    private bagService: BagService,
  ) {}

  async login(userDto: UserDto) {
    const user = await this.validateUser(userDto);
    const userDataToSend = new SendUserDto(user);

    const tokens = this.tokenService.generateTokens(userDataToSend);
    await this.tokenService.saveToken(user.id, tokens.refreshToken);

    return { tokens, user: userDataToSend };
  }

  async register(userDto: CreateUserDto) {
    const user = await this.userService.getUserByUsername(userDto.username);

    if (user) {
      throw new HttpException(
        `The user with username ${userDto.username} already exists`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const hashPassword = await hash(String(userDto.password), 6);
    const newUser = await this.userService.createUser({
      ...userDto,
      password: hashPassword,
    });

    if (!newUser) {
      throw new HttpException('Registration failed', HttpStatus.BAD_REQUEST);
    }

    await this.bagService.createBag(newUser.id);

    const userWithBag = await this.userService.getUser(newUser.id);
    const userDataToSend = new SendUserDto(userWithBag);
    const tokens = this.tokenService.generateTokens(userDataToSend);

    return { tokens, user: userDataToSend };
  }

  async logout(refreshToken: string) {
    const rowsRemoved = await this.tokenService.removeToken(refreshToken);

    if (rowsRemoved === 0) {
      throw new Error('Failed to delete token');
    }

    return rowsRemoved;
  }

  private async validateUser(userDto: UserDto) {
    const user = await this.userService.getUserByUsername(userDto.username);

    if (!user) {
      throw new HttpException(
        `User with username ${userDto.username} was not found`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const passwordsEqual = await compare(userDto.password, user.password);

    if (passwordsEqual) {
      return user;
    }

    throw new UnauthorizedException({
      message: 'Incorrent username or password',
    });
  }
}
