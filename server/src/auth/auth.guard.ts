import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { TokenService } from 'src/token/token.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private tokenService: TokenService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();

    try {
      const authToken = req.cookies['JWT_ACCESS_TOKEN'] || '';

      const user = this.tokenService.validateAuthToken(authToken);

      req.user = user;

      return true;
    } catch (err) {
      throw new UnauthorizedException({ message: 'User is not authorized' });
    }
  }
}
