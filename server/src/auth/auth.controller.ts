import { Body, Controller, Post, Req, Res } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserDto } from 'src/users/dto/user.dto';
import { AuthService } from './auth.service';

const MONTH_IN_MS = 30 * 24 * 60 * 60 * 1000;
const ONE_MINUTE_MS = 60000;
const ACCESS_EXPIRATION_TIME = 60 * 60 * 1000;

@ApiTags('Authorization')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Login' })
  @ApiResponse({ status: 200 })
  @Post('/login')
  async login(
    @Body() loginDto: UserDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    const cookieData = await this.authService.login(loginDto);
    const expireTime = new Date(Date.now() + MONTH_IN_MS);

    response.cookie(
      'JWT_REFRESH_TOKEN',
      `Bearer ${cookieData.tokens.refreshToken}`,
      {
        secure: false,
        httpOnly: true,
        expires: expireTime,
      },
    );
    response.cookie(
      'JWT_ACCESS_TOKEN',
      `Bearer ${cookieData.tokens.accessToken}`,
      {
        secure: false,
        httpOnly: true,
        maxAge: ACCESS_EXPIRATION_TIME,
      },
    );
    response.cookie('user', JSON.stringify(cookieData.user), {
      secure: false,
      httpOnly: true,
      expires: expireTime,
    });
    response.cookie('REFRESH_TOKEN_LIFE_EXP', JSON.stringify(expireTime), {
      secure: false,
      maxAge: MONTH_IN_MS + ONE_MINUTE_MS,
    });
    return {
      user: cookieData.user,
      accessToken: `Bearer ${cookieData.tokens.accessToken}`,
    };
  }

  @ApiOperation({ summary: 'Register' })
  @ApiResponse({ status: 200 })
  @Post('/register')
  async register(
    @Body() registerDto: CreateUserDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    const cookieData = await this.authService.register(registerDto);
    const expireTime = new Date(Date.now() + MONTH_IN_MS);

    response.cookie('REFRESH_TOKEN_LIFE_EXP', JSON.stringify(expireTime), {
      secure: false,
      maxAge: MONTH_IN_MS + ONE_MINUTE_MS,
    });
    response.cookie(
      'JWT_ACCESS_TOKEN',
      `Bearer ${cookieData.tokens.accessToken}`,
      {
        secure: false,
        httpOnly: true,
        maxAge: ACCESS_EXPIRATION_TIME,
      },
    );
    response.cookie(
      'JWT_REFRESH_TOKEN',
      `Bearer ${cookieData.tokens.refreshToken}`,
      {
        secure: false,
        httpOnly: true,
        expires: expireTime,
      },
    );
    response.cookie('user', JSON.stringify(cookieData.user), {
      secure: false,
      httpOnly: true,
      expires: expireTime,
    });

    return {
      user: cookieData.user,
      accessToken: `Bearer ${cookieData.tokens.accessToken}`,
    };
  }

  @ApiOperation({ summary: 'Logout' })
  @ApiResponse({ status: 200 })
  @Post('/logout')
  async logout(
    @Req() req: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    const { JWT_REFRESH_TOKEN } = req.cookies;
    await this.authService.logout(JWT_REFRESH_TOKEN);

    response.cookie('JWT_ACCESS_TOKEN', null, {
      secure: false,
      httpOnly: true,
      maxAge: 0,
    });
    response.cookie('JWT_REFRESH_TOKEN', null, {
      secure: false,
      httpOnly: true,
      maxAge: 0,
    });
    response.cookie('user', null, {
      secure: false,
      httpOnly: true,
      maxAge: 0,
    });
    response.cookie('user-token', null, {
      secure: false,
      httpOnly: true,
      maxAge: 0,
    });

    return { message: 'User was log out' };
  }
}
