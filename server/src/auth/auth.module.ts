import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { BagModule } from 'src/bag/bag.module';
import { TokenModule } from 'src/token/token.module';

@Module({
  providers: [AuthService],
  controllers: [AuthController],
  imports: [forwardRef(() => UsersModule), TokenModule, BagModule],
  exports: [AuthService, TokenModule],
})
export class AuthModule {}
