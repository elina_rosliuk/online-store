import { Controller, Get, Req, Res } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { TokenService } from './token.service';

const MONTH_IN_MS = 30 * 24 * 60 * 60 * 1000;
const ONE_MINUTE_MS = 60000;
const ACCESS_EXPIRATION_TIME = 60 * 60 * 1000;

@ApiTags('Token')
@Controller('token')
export class TokenController {
  constructor(private tokenService: TokenService) {}

  @ApiOperation({ summary: 'Refresh token' })
  @ApiResponse({ status: 200 })
  @Get('/refresh')
  async getRefreshToken(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    const cookies = req.cookies;

    const cookieData = await this.tokenService.refresh(
      cookies['JWT_REFRESH_TOKEN'],
    );

    const expireTime =
      JSON.parse(cookies['REFRESH_TOKEN_LIFE_EXP']) || Date.now() + MONTH_IN_MS;

    if (!cookies['REFRESH_TOKEN_LIFE_EXP']) {
      res.cookie(
        'REFRESH_TOKEN_LIFE_EXP',
        JSON.stringify(new Date(Date.now() + MONTH_IN_MS)),
        {
          secure: false,
          maxAge: MONTH_IN_MS + ONE_MINUTE_MS,
        },
      );
    }
    res.cookie(
      'JWT_REFRESH_TOKEN',
      `Bearer ${cookieData.tokens.refreshToken}`,
      {
        secure: false,
        httpOnly: true,
        expires: new Date(expireTime),
      },
    );
    res.cookie('JWT_ACCESS_TOKEN', `Bearer ${cookieData.tokens.accessToken}`, {
      secure: false,
      httpOnly: true,
      maxAge: ACCESS_EXPIRATION_TIME,
    });
    res.cookie('user', JSON.stringify(cookieData.user), {
      secure: false,
      httpOnly: true,
      expires: new Date(expireTime),
    });
    return {
      user: cookieData.user,
    };
  }
}
