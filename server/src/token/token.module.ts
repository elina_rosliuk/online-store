import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { SequelizeModule } from '@nestjs/sequelize';
import { UsersModule } from 'src/users/users.module';
import { TokenController } from './token.controller';
import { Token } from './token.model';
import { TokenService } from './token.service';

@Module({
  controllers: [TokenController],
  providers: [TokenService],
  imports: [
    SequelizeModule.forFeature([Token]),
    JwtModule.register({}),
    forwardRef(() => UsersModule),
  ],
  exports: [TokenService, JwtModule],
})
export class TokenModule {}
