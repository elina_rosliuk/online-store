import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/sequelize';
import { SendUserDto } from 'src/users/dto/sendUser.dto';
import { UserDto } from 'src/users/dto/user.dto';
import { User } from 'src/users/users.model';
import { UsersService } from 'src/users/users.service';
import { Token } from './token.model';

@Injectable()
export class TokenService {
  constructor(
    @InjectModel(Token) private tokenRepository: typeof Token,
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async refresh(refreshToken: string) {
    if (!refreshToken) {
      throw new UnauthorizedException();
    }

    const tokenBody = refreshToken.split(' ')[1];

    if (!tokenBody) {
      throw new UnauthorizedException();
    }

    const userData = this.validateRefreshToken(tokenBody);
    const tokenFromDb = await this.findToken(tokenBody);

    if (!userData || !tokenFromDb) {
      throw new UnauthorizedException();
    }

    const user = await this.usersService.getUser(userData.id);
    const userDataToSend = new SendUserDto(user);

    const tokens = this.generateTokens(userDataToSend);
    this.removeToken(tokenBody);
    this.saveToken(userDataToSend.id, tokens.refreshToken);

    return { tokens, user: userDataToSend };
  }

  validateAccessToken(token: string) {
    try {
      const userData = this.jwtService.verify(token, {
        secret: process.env.JWT_ACCESS_KEY,
      });

      return userData;
    } catch (err) {
      console.log(err);
    }
  }

  validateRefreshToken(token: string) {
    try {
      const userData = this.jwtService.verify(token, {
        secret: process.env.JWT_REFRESH_KEY,
      });

      return userData;
    } catch (err) {
      return console.log(err);
    }
  }

  generateTokens(payload: Partial<UserDto>) {
    const accessToken = this.jwtService.sign(
      { ...payload },
      {
        secret: process.env.JWT_ACCESS_KEY,
        expiresIn: '1h',
      },
    );
    const refreshToken = this.jwtService.sign(
      { ...payload },
      {
        secret: process.env.JWT_REFRESH_KEY,
        expiresIn: '30d',
      },
    );

    return {
      accessToken,
      refreshToken,
    };
  }

  async saveToken(userId: number, refreshToken: string) {
    const tokenData = await this.tokenRepository.findOne({ where: { userId } });

    if (tokenData) {
      const updatedData = await tokenData.update({ refreshToken });
      return updatedData;
    }

    const newToken = await this.tokenRepository.create({
      refreshToken,
      userId,
    });

    return newToken;
  }

  async removeToken(refreshToken = '') {
    const tokenBody = refreshToken.split(' ')[1];

    if (!refreshToken || !tokenBody) {
      throw new Error('No token provided');
    }

    const tokenData = await this.tokenRepository.destroy({
      where: { refreshToken: tokenBody },
    });

    return tokenData;
  }

  async findToken(refreshToken: string) {
    const token = await this.tokenRepository.findOne({
      where: { refreshToken },
    });

    return token;
  }

  validateAuthToken(authToken: string): Promise<User> {
    if (!authToken) {
      throw new UnauthorizedException({ message: 'User is not authorized' });
    }

    const bearer = authToken.split(' ')[0];
    const token = authToken.split(' ')[1];

    if (bearer !== 'Bearer' || !token) {
      throw new UnauthorizedException({ message: 'User is not authorized' });
    }

    const user = this.validateAccessToken(token);

    if (!user) {
      throw new UnauthorizedException({ message: 'User is not authorized' });
    }

    return user;
  }
}
