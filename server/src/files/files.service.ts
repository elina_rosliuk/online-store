import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as path from 'path';
import * as fs from 'fs';
import * as asyncfs from 'fs/promises';
import * as uuid from 'uuid';

@Injectable()
export class FilesService {
  imageExtensions: string[] = ['jpg', 'jpeg', 'png'];

  async createFile(file): Promise<string> {
    try {
      const fileExtension = file.originalname.split('.').pop();

      if (!this.validateImage(fileExtension)) {
        throw new HttpException(
          `Allowed only files with ${this.imageExtensions.join(
            ', ',
          )} extensions`,
          HttpStatus.BAD_REQUEST,
        );
      }

      const fileName = uuid.v4() + '.' + fileExtension;
      const filePath = path.resolve(__dirname, '..', 'static');

      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }

      fs.writeFileSync(path.join(filePath, fileName), file.buffer);

      return fileName;
    } catch (err) {
      console.log(err);
      throw new HttpException(
        'Error while creating file',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async deleteFile(filename: string): Promise<boolean> {
    try {
      const filePath = path.resolve(__dirname, '..', 'static', filename);

      await asyncfs.access(filePath);
      await asyncfs.rm(filePath);

      return true;
    } catch (err) {
      console.log(err);
    }
  }

  private validateImage(fileExtension: string) {
    if (this.imageExtensions.includes(fileExtension)) {
      return true;
    }

    return false;
  }
}
