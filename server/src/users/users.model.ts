import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  HasMany,
  HasOne,
} from 'sequelize-typescript';
import { Bag } from 'src/bag/bag.model';
import { Rating } from 'src/rating/rating.model';
import { Token } from 'src/token/token.model';

export enum UserRole {
  CUSTOMER = 'customer',
  ADMIN = 'admin',
}

interface UserCreationAttrs {
  username: string;
  password: string;
  role?: UserRole;
}

@Table({ tableName: 'users' })
export class User extends Model<User, UserCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'user001' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  username: string;

  @ApiProperty({ example: 'abc123456' })
  @Column({ type: DataType.STRING, allowNull: false })
  password: string;

  @ApiProperty({ example: 'customer' })
  @Column({ type: DataType.TEXT, defaultValue: UserRole.CUSTOMER })
  role: UserRole;

  @ApiProperty({ example: false, required: false })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isDeleted: boolean;

  @HasMany(() => Rating)
  rates: Rating[];

  @HasOne(() => Bag)
  bag: Bag;

  @HasOne(() => Token)
  refreshToken: Token;
}
