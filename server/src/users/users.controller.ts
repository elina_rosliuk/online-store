import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { AuthGuard } from 'src/auth/auth.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { QueryUsersDto } from './dto/query-users.dto';
import { UpdateUserRoleDto } from './dto/update-role.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Role } from './roles.decorator';
import { RolesGuard } from './roles.guard';
import { User, UserRole } from './users.model';
import { UsersService } from './users.service';

interface RequestWithUser extends Request {
  user: User;
}

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @ApiOperation({ summary: 'Create User' })
  @ApiResponse({ status: 200, type: User })
  @Post()
  create(@Body() userDto: CreateUserDto) {
    return this.usersService.createUser(userDto);
  }

  @ApiOperation({ summary: 'Get Me' })
  @ApiResponse({ status: 200, type: User })
  @UseGuards(AuthGuard)
  @Get('me')
  getMe(@Req() req: RequestWithUser) {
    return { user: req.user };
  }

  @ApiOperation({ summary: 'Get All Users' })
  @ApiResponse({ status: 200, type: User })
  @Get()
  getAllUsers(@Query() dto: QueryUsersDto) {
    const { limit = 5, page = 1, filter = 'all' } = dto;
    return this.usersService.getAllUsers(limit, page, filter);
  }

  @ApiOperation({ summary: 'Get User' })
  @ApiResponse({ status: 200, type: User })
  @UseGuards(RolesGuard)
  @Get(':id')
  getUser(@Param('id') id: string) {
    return this.usersService.getUser(id);
  }

  @ApiOperation({ summary: 'Update username' })
  @ApiResponse({ status: 200, type: User })
  @UseGuards(AuthGuard)
  @Patch(':id')
  updateUser(@Param('id') id: string, @Body() userDto: UpdateUserDto) {
    return this.usersService.updateUser(id, userDto);
  }

  @ApiOperation({ summary: 'Change user role' })
  @ApiResponse({ status: 200, type: User })
  @Role(UserRole.ADMIN)
  @UseGuards(RolesGuard)
  @Patch('/role/:id')
  updateUserRole(
    @Param('id') id: string,
    @Body() userRoleDto: UpdateUserRoleDto,
  ) {
    return this.usersService.updateUserRole(id, userRoleDto);
  }

  @ApiOperation({ summary: 'Delete User' })
  @ApiResponse({ status: 200 })
  @UseGuards(AuthGuard)
  @Delete(':id')
  deleteUser(@Param('id') id: string) {
    return this.usersService.softDeleteUser(id);
  }
}
