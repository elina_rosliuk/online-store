import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthModule } from 'src/auth/auth.module';
import { Bag } from 'src/bag/bag.model';
import { BagModule } from 'src/bag/bag.module';
import { Rating } from 'src/rating/rating.model';
import { TokenModule } from 'src/token/token.module';
import { UsersController } from './users.controller';
import { User } from './users.model';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [
    SequelizeModule.forFeature([User, Rating, Bag]),
    forwardRef(() => AuthModule),
    forwardRef(() => TokenModule),
    BagModule,
  ],
  exports: [UsersService],
})
export class UsersModule {}
