import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { col, fn, Op, where } from 'sequelize';
import { BagService } from 'src/bag/bag.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserRoleDto } from './dto/update-role.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './users.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User) private userRepository: typeof User,
    private bagService: BagService,
  ) {}

  async createUser(dto: CreateUserDto) {
    const existingUser = await this.getUserByUsername(dto.username);

    // TODO: Validate for uniqueness on the stage of validation
    if (existingUser) {
      throw new HttpException(
        'Such user already exists',
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = await this.userRepository.create(dto);
    return user;
  }

  async getUser(id: string | number) {
    const user = await this.userRepository.findByPk(id, {
      include: { all: true },
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    return user;
  }

  async updateUser(id: string, dto: UpdateUserDto) {
    const user = await this.userRepository.findByPk(id);

    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    try {
      await user.update({ username: dto.username });
      await user.save();
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }

    return user;
  }

  async getAllUsers(limit = 5, page = 1, filter = 'all') {
    try {
      const offset = limit * (page - 1);
      const roleFilter = filter === 'all' ? '' : filter;

      const users = await this.userRepository.findAndCountAll({
        where: {
          role: where(fn('LOWER', col('role')), 'LIKE', '%' + roleFilter + '%'),
          isDeleted: false,
        },
        attributes: {
          exclude: ['updatedAt', 'password'],
        },
        order: [['id', 'ASC']],
        limit,
        offset,
        distinct: true,
      });

      if (roleFilter) {
        const filterCount = await this.userRepository.count({
          where: {
            role: where(
              fn('LOWER', col('role')),
              'LIKE',
              '%' + roleFilter + '%',
            ),
            isDeleted: false,
          },
        });

        users.count = filterCount;
      }

      return users;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async updateUserRole(id: string, dto: UpdateUserRoleDto) {
    const user = await this.getUser(id);

    try {
      await user.update({ role: dto.role });
      await user.save();
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }

    return user;
  }

  async softDeleteUser(id: string) {
    const user = await this.userRepository.findByPk(id);

    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    if (user.isDeleted) {
      throw new HttpException(
        'User is already deleted',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      await user.update({ isDeleted: true });
      await this.bagService.deleteBag(user.bag.id);
      await user.save();
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }

    return { message: 'Success' };
  }

  async getUserByUsername(username: string) {
    const user = await this.userRepository.findOne({
      where: { username },
      include: { all: true },
    });

    return user;
  }
}
