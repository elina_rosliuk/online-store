import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UserDto {
  @ApiProperty({ example: 'ann' })
  @IsString({ message: 'Username should be a string' })
  @IsNotEmpty({ message: 'Username is required' })
  readonly username: string;

  @ApiProperty({ example: '123456' })
  readonly password: string;
}
