import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { UserRole } from '../users.model';

export class UpdateUserRoleDto {
  @ApiProperty({ example: 'admin' })
  @IsString({ message: 'User role should be a string' })
  @IsEnum(UserRole, { message: 'Such user role does not exist' })
  @IsNotEmpty({ message: 'User role is required' })
  readonly role: UserRole;
}
