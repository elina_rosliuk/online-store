import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlphanumeric,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { UserRole } from '../users.model';

export class CreateUserDto {
  @ApiProperty({ example: 'ann' })
  @IsString({ message: 'Username should be a string' })
  @IsNotEmpty({ message: 'Username is required' })
  readonly username: string;

  @ApiProperty({ example: '123456' })
  @IsAlphanumeric()
  @IsNotEmpty({ message: 'Password is required' })
  @MinLength(6, {
    each: true,
    message: 'Password is too short. Minimal length is 6 characters',
  })
  @MaxLength(50, {
    each: true,
    message: 'Password is too long. Maximal length is 50 characters',
  })
  readonly password: string;

  @ApiProperty({ example: 'customer' })
  @IsOptional()
  @IsEnum(UserRole, { message: 'There is no such user role' })
  readonly role?: UserRole;
}
