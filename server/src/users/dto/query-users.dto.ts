import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsOptional } from 'class-validator';

export enum UsersFilter {
  CUSTOMER = 'customer',
  ADMIN = 'admin',
  ALL = 'all',
}

export class QueryUsersDto {
  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly limit: number;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly page: number;

  @ApiProperty({ example: 'users' })
  @IsOptional()
  @IsEnum(UsersFilter, { message: 'Should be a customer, admin or all' })
  readonly filter: UsersFilter;
}
