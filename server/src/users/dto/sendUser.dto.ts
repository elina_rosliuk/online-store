import { User, UserRole } from '../users.model';

export class SendUserDto {
  username: string;
  id: number;
  bagId: number;
  role: UserRole;

  constructor(model: User) {
    this.id = model.id;
    this.username = model.username;
    this.role = model.role;
    this.bagId = model?.bag.id;
  }
}
