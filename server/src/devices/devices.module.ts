import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Brand } from 'src/brands/brands.model';
import { BrandsModule } from 'src/brands/brands.module';
import { FilesModule } from 'src/files/files.module';
import { Rating } from 'src/rating/rating.model';
import { RatingModule } from 'src/rating/rating.module';
import { Type } from 'src/types/types.model';
import { TypesModule } from 'src/types/types.module';
import { DevicesController } from './devices.controller';
import { Device } from './devices.model';
import { DevicesService } from './devices.service';

@Module({
  controllers: [DevicesController],
  providers: [DevicesService],
  imports: [
    forwardRef(() => RatingModule),
    SequelizeModule.forFeature([Device, Brand, Type, Rating]),
    FilesModule,
    BrandsModule,
    TypesModule,
  ],
  exports: [DevicesService],
})
export class DevicesModule {}
