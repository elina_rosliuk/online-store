import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class QueryDeviceDto {
  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly limit: number;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly page: number;

  @ApiProperty({ example: 'iPhone' })
  @IsOptional()
  @IsString({ message: 'Should be a string' })
  readonly search: string;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly type: number;
}
