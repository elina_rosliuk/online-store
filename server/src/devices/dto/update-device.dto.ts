import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateDeviceDto {
  @ApiProperty({ example: 'IPhone 12 Pro Max' })
  @IsOptional()
  @IsString({ message: 'Should be a string' })
  readonly title: string;

  @ApiProperty({
    example:
      'The A14 Bionic processor is ahead of all other iPhone processors. The Pro camera system delivers amazing quality photos taken in low light conditions, and provides even more shooting options on the iPhone 12 Pro Max.',
  })
  @IsOptional()
  @IsString({ message: 'Should be a string' })
  readonly description: string;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly quantity: number;

  @ApiProperty({ example: 105.05 })
  @IsOptional()
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly price: number;
}
