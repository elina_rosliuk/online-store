import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateDeviceDto {
  @ApiProperty({ example: 'IPhone 12 Pro Max' })
  @IsString({ message: 'Should be a string' })
  @IsNotEmpty({ message: 'The field is required' })
  readonly title: string;

  @ApiProperty({
    example:
      'The A14 Bionic processor is ahead of all other iPhone processors. The Pro camera system delivers amazing quality photos taken in low light conditions, and provides even more shooting options on the iPhone 12 Pro Max.',
  })
  @IsString({ message: 'Should be a string' })
  readonly description: string;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly quantity: number;

  @ApiProperty({ example: 105.05 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly price: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly brandId: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly typeId: number;
}
