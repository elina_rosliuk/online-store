import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Express } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Device } from './devices.model';
import { DevicesService } from './devices.service';
import { CreateDeviceDto } from './dto/create-device.dto';
import { RatingService } from 'src/rating/rating.service';
import { Rating } from 'src/rating/rating.model';
import { CreateRatingDto } from 'src/rating/dto/create-rating.dto';
import { UpdateDeviceDto } from './dto/update-device.dto';
import { QueryDeviceDto } from './dto/query.dto';

@ApiTags('Devices')
@Controller('devices')
export class DevicesController {
  constructor(
    private devicesService: DevicesService,
    private ratingService: RatingService,
  ) {}

  @ApiOperation({ summary: 'Create Device' })
  @ApiResponse({ status: 200, type: Device })
  @UseInterceptors(FileInterceptor('image'))
  @Post()
  create(
    @Body() deviceDto: CreateDeviceDto,
    @UploadedFile() image: Express.Multer.File,
  ) {
    return this.devicesService.createDevice(deviceDto, image);
  }

  @ApiOperation({ summary: 'Set Rate' })
  @ApiResponse({ status: 200, type: Rating })
  @Post(':id/rating')
  setRate(@Body() ratingDto: CreateRatingDto, @Param('id') deviceId: string) {
    return this.ratingService.setRate(ratingDto, deviceId);
  }

  @ApiOperation({ summary: 'Get All Devices' })
  @ApiResponse({ status: 200, type: [Device] })
  @Get()
  getAll(@Query() dto: QueryDeviceDto) {
    const { limit = 10, page = 1, search = '', type } = dto;
    return this.devicesService.getAllDevices(limit, page, search, type);
  }

  @ApiOperation({ summary: 'Get Device' })
  @ApiResponse({ status: 200, type: Device })
  @Get(':id')
  get(@Param('id') id: string) {
    return this.devicesService.getDeviceById(id);
  }

  @ApiOperation({ summary: 'Delete Device' })
  @ApiResponse({ status: 200 })
  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.devicesService.deleteDevice(id);
  }

  @ApiOperation({ summary: 'Update device info' })
  @ApiResponse({ status: 200, type: Device })
  @Put(':id')
  updateDeviceInfo(
    @Param('id') id: string,
    @Body() deviceDto: UpdateDeviceDto,
  ) {
    return this.devicesService.updateDeviceInfo(id, deviceDto);
  }

  @ApiOperation({ summary: 'Update device picture' })
  @ApiResponse({ status: 200, type: Device })
  @UseInterceptors(FileInterceptor('image'))
  @Patch(':id/image')
  updatePicture(
    @Param('id') id: string,
    @UploadedFile() image: Express.Multer.File,
  ) {
    return this.devicesService.updateDevicePicture(id, image);
  }
}
