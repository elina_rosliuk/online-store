import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
  HasOne,
} from 'sequelize-typescript';
import { Brand } from 'src/brands/brands.model';
import { DeviceBag } from 'src/device-bag/device-bag.model';
import { Rating } from 'src/rating/rating.model';
import { Type } from 'src/types/types.model';

interface DeviceCreationAttrs {
  title: string;
  description: string;
  quantity: number;
  price: number;
  image?: string;
}

@Table({ tableName: 'devices' })
export class Device extends Model<Device, DeviceCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'iPhone 12 Pro Max' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  title: string;

  @ApiProperty({
    example:
      'The A14 Bionic processor is ahead of all other iPhone processors. The Pro camera system delivers amazing quality photos taken in low light conditions, and provides even more shooting options on the iPhone 12 Pro Max.',
  })
  @Column({ type: DataType.TEXT })
  description: string;

  @ApiProperty({ example: 1 })
  @Column({ type: DataType.INTEGER, defaultValue: 0 })
  quantity: number;

  @ApiProperty({ example: 0.01 })
  @Column({ type: DataType.FLOAT, defaultValue: 0.01 })
  price: number;

  @ApiProperty({ example: 'iphone_12_pro_max_gray.png' })
  @Column({ type: DataType.STRING })
  image: string;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Brand)
  @Column({
    type: DataType.INTEGER,
  })
  brandId: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Type)
  @Column({
    type: DataType.INTEGER,
  })
  typeId: number;

  @BelongsTo(() => Brand)
  brand: Brand;

  @BelongsTo(() => Type)
  type: Type;

  @HasMany(() => Rating)
  rates: Rating[];

  @HasOne(() => DeviceBag)
  deviceInBag: DeviceBag;
}
