import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op, fn, where, col } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';
import { BrandsService } from 'src/brands/brands.service';
import { FilesService } from 'src/files/files.service';
import { Rating } from 'src/rating/rating.model';
import { TypesService } from 'src/types/types.service';
import { Device } from './devices.model';
import { CreateDeviceDto } from './dto/create-device.dto';
import { UpdateDeviceDto } from './dto/update-device.dto';

@Injectable()
export class DevicesService {
  constructor(
    @InjectModel(Device) private devicesRepository: typeof Device,
    private filesService: FilesService,
    private brandsService: BrandsService,
    private typesService: TypesService,
    private sequelize: Sequelize,
  ) {}

  private async checkDeviceForUniqueness(title: string, id?: number | string) {
    const existingDevice = await this.getDeviceByName(title);

    // TODO: Validate for uniqueness on the stage of validation
    if (
      (!id && existingDevice) ||
      (id && existingDevice && Number(existingDevice.id) !== Number(id))
    ) {
      throw new HttpException(
        'Device with such title already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async createDevice(deviceDto: CreateDeviceDto, image: any) {
    try {
      await this.checkDeviceForUniqueness(deviceDto.title);

      const fileName = await this.filesService.createFile(image);
      const brand = await this.brandsService.getBrandById(deviceDto.brandId);
      const type = await this.typesService.getTypeById(deviceDto.typeId);

      const device = await this.sequelize.transaction(async (t) => {
        const transactionHost = { transaction: t };

        await brand.$add('types', type, transactionHost);
        await type.$add('brands', brand, transactionHost);

        const newDevice = await this.devicesRepository.create(
          {
            ...deviceDto,
            image: fileName,
          },
          transactionHost,
        );

        return newDevice;
      });

      return device;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async getAllDevices(limit = 10, page = 1, search = '', type = null) {
    try {
      const offset = limit * (page - 1);
      const lowerSearch = search.toLowerCase();

      const devices = await this.devicesRepository.findAndCountAll({
        where: {
          title: where(
            fn('LOWER', col('title')),
            'LIKE',
            '%' + lowerSearch + '%',
          ),
          typeId: type || { [Op.ne]: null },
        },
        order: [['id', 'ASC']],
        limit,
        offset,
        include: { model: Rating, as: 'rates' },
        distinct: true,
      });

      if (search) {
        const searchCount = await this.devicesRepository.count({
          where: {
            title: where(
              fn('LOWER', col('title')),
              'LIKE',
              '%' + lowerSearch + '%',
            ),
          },
        });

        devices.count = searchCount;
      }

      return devices;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async getDeviceById(id: number | string) {
    try {
      const device = await this.devicesRepository.findByPk(id, {
        include: { model: Rating, as: 'rates' },
      });

      if (!device) {
        throw new HttpException('Device is not found', HttpStatus.BAD_REQUEST);
      }

      return device;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  private async getDeviceByName(title: string) {
    const device = await this.devicesRepository.findOne({
      where: { title },
    });

    return device;
  }

  async deleteDevice(id: number | string) {
    try {
      const device = await this.getDeviceById(id);

      if (device.image) {
        await this.filesService.deleteFile(device.image);
      }

      const rowsDeleted = await this.devicesRepository.destroy({
        where: { id },
      });

      return rowsDeleted > 0 && { message: 'Device was successfully deleted' };
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async updateDevicePicture(id: number | string, image: any) {
    try {
      const device = await this.getDeviceById(id);

      if (!image) {
        throw new HttpException('Image is required', HttpStatus.BAD_REQUEST);
      }

      const oldImage = device.image;
      const fileName = await this.filesService.createFile(image);
      const updatedDevice = await device.update({ image: fileName });

      if (oldImage) {
        await this.filesService.deleteFile(oldImage);
      }

      return updatedDevice;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async updateDeviceInfo(id: number | string, deviceDto: UpdateDeviceDto) {
    try {
      const device = await this.getDeviceById(id);

      if (deviceDto.title) {
        await this.checkDeviceForUniqueness(deviceDto.title, id);
      }

      console.log('deviceDto', deviceDto);

      const updatedDevice = await device.update({ ...deviceDto });

      return updatedDevice;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async changeDeviceQuantity(
    id: number | string,
    operation: '-' | '+',
    num: number,
  ) {
    try {
      const device = await this.getDeviceById(id);

      if (!device) {
        throw new HttpException(
          'No device with such id found',
          HttpStatus.BAD_REQUEST,
        );
      }

      let updatedDevice: Device;

      switch (operation) {
        case '+':
          updatedDevice = await device.update({
            quantity: device.quantity + num,
          });
          break;
        case '-':
          updatedDevice = await device.update({
            quantity: device.quantity - num,
          });
          break;
        default:
          break;
      }

      return updatedDevice;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }
}
