import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Bag } from 'src/bag/bag.model';
import { Device } from 'src/devices/devices.model';
import { DevicesModule } from 'src/devices/devices.module';
import { DeviceBag } from './device-bag.model';
import { DeviceBagService } from './device-bag.service';

@Module({
  providers: [DeviceBagService],
  imports: [
    SequelizeModule.forFeature([DeviceBag, Device, Bag]),
    DevicesModule,
  ],
  exports: [DeviceBagService],
})
export class DeviceBagModule {}
