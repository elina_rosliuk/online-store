import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Bag } from 'src/bag/bag.model';
import { Device } from 'src/devices/devices.model';

interface DeviceBagCreationAttrs {
  bagId: number;
  deviceId: number;
}

@Table({ tableName: 'device-bag' })
export class DeviceBag extends Model<DeviceBag, DeviceBagCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Bag)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  bagId: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Device)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  deviceId: number;

  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 1,
  })
  count: number;

  @BelongsTo(() => Bag)
  bag: Bag;

  @BelongsTo(() => Device)
  device: Device;
}
