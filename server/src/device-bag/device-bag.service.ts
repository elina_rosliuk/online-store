import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { DevicesService } from 'src/devices/devices.service';
import { DeviceBag } from './device-bag.model';

@Injectable()
export class DeviceBagService {
  constructor(
    @InjectModel(DeviceBag) private deviceInBagRepository: typeof DeviceBag,
    private devicesService: DevicesService,
  ) {}

  async addToBag(bagId: number, deviceId: number) {
    try {
      const device = await this.devicesService.getDeviceById(deviceId);

      if (device.quantity < 1) {
        throw new HttpException(
          'All devices are sold out at the moment',
          HttpStatus.BAD_REQUEST,
        );
      }

      let deviceInBag = await this.deviceInBagRepository.findOne({
        where: { deviceId, bagId },
      });

      if (deviceInBag) {
        await deviceInBag.update({
          count: deviceInBag.count + 1,
        });
      } else {
        deviceInBag = await this.deviceInBagRepository.create({
          deviceId,
          bagId,
        });
      }

      await this.devicesService.changeDeviceQuantity(deviceId, '-', 1);

      return deviceInBag;
    } catch (err) {
      console.log(err);
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  async removeFromBag(deviceId: number, bagId: number) {
    try {
      const deviceInBag = await this.deviceInBagRepository.findOne({
        where: { deviceId, bagId },
      });

      let devicesRemoved = 0;

      if (deviceInBag && deviceInBag.count > 1) {
        await deviceInBag.update({ count: deviceInBag.count - 1 });
        devicesRemoved = 1;
      } else {
        devicesRemoved = await this.deviceInBagRepository.destroy({
          where: { bagId, deviceId },
        });
      }

      await this.devicesService.changeDeviceQuantity(deviceId, '+', 1);

      return devicesRemoved > 0;
    } catch (err) {
      console.log(err);
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }
}
