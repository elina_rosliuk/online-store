import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';

export class RemoveDeviceFromBagDto {
  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'The field is required' })
  @Type(() => Number)
  readonly product: number;
}
