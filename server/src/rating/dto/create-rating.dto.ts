import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class CreateRatingDto {
  @ApiProperty({ example: 4 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly rate: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsNumber({}, { message: 'Should be a number' })
  readonly userId: number;
}
