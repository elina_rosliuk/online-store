import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { DevicesService } from 'src/devices/devices.service';
import { CreateRatingDto } from './dto/create-rating.dto';
import { Rating } from './rating.model';

@Injectable()
export class RatingService {
  constructor(
    @InjectModel(Rating) private ratingRepository: typeof Rating,
    private devicesService: DevicesService,
  ) {}

  async setRate(ratingDto: CreateRatingDto, deviceId: string) {
    const { userId } = ratingDto;

    await this.devicesService.getDeviceById(deviceId);

    const ratingFromUser = await this.ratingRepository.findOne({
      where: { userId, deviceId },
    });

    if (ratingFromUser) {
      throw new HttpException(
        'The user already set his rate',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      const rate = await this.ratingRepository.create({
        ...ratingDto,
        deviceId: Number(deviceId),
      });

      return rate;
    } catch (err) {
      console.log(err);

      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
