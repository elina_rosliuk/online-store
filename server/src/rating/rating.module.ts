import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Device } from 'src/devices/devices.model';
import { DevicesModule } from 'src/devices/devices.module';
import { User } from 'src/users/users.model';
import { Rating } from './rating.model';
import { RatingService } from './rating.service';

@Module({
  providers: [RatingService],
  imports: [
    forwardRef(() => DevicesModule),
    SequelizeModule.forFeature([Rating, User, Device]),
  ],
  exports: [RatingService],
})
export class RatingModule {}
