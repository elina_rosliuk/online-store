import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Device } from 'src/devices/devices.model';
import { User } from 'src/users/users.model';

interface RatingCreationAttrs {
  rate: number;
  userId: number;
  deviceId: number;
}

@Table({ tableName: 'rating' })
export class Rating extends Model<Rating, RatingCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 5 })
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  rate: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  userId: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Device)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  deviceId: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Device)
  device: Device;
}
