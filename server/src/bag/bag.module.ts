import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from 'src/users/users.model';
import { Bag } from 'src/bag/bag.model';
import { BagController } from './bag.controller';
import { BagService } from './bag.service';
import { DeviceBagModule } from 'src/device-bag/device-bag.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  controllers: [BagController],
  providers: [BagService],
  imports: [
    SequelizeModule.forFeature([Bag, User]),
    DeviceBagModule,
    forwardRef(() => AuthModule),
  ],
  exports: [BagService],
})
export class BagModule {}
