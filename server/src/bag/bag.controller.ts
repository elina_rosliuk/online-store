import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AddDeviceToBagDto } from 'src/device-bag/dto/add-device-to-bag.dto';
import { DeviceBag } from 'src/device-bag/device-bag.model';
import { BagService } from './bag.service';
import { RemoveDeviceFromBagDto } from 'src/device-bag/dto/remove-device-from-bag.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@ApiTags('Bag')
@Controller('bag')
export class BagController {
  constructor(private bagService: BagService) {}

  @ApiOperation({ summary: 'Get devices in bag' })
  @ApiResponse({ status: 200, type: DeviceBag })
  @UseGuards(AuthGuard)
  @Get(':id')
  get(@Param('id') bagId: string) {
    return this.bagService.getDevicesInBag(Number(bagId));
  }

  @ApiOperation({ summary: 'Add product to bag' })
  @ApiResponse({ status: 200, type: DeviceBag })
  @UseGuards(AuthGuard)
  @Post()
  create(@Body() dto: AddDeviceToBagDto) {
    return this.bagService.addToBag(dto);
  }

  @ApiOperation({ summary: 'Remove product from bag' })
  @ApiResponse({ status: 200 })
  @UseGuards(AuthGuard)
  @Delete(':id')
  delete(
    @Param('id', ParseIntPipe) bagId: number,
    @Query() query: RemoveDeviceFromBagDto,
  ) {
    const { product } = query;
    return this.bagService.removeFromBag(product, bagId);
  }
}
