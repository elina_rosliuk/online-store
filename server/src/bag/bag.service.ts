import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { AddDeviceToBagDto } from 'src/device-bag/dto/add-device-to-bag.dto';
import { DeviceBag } from 'src/device-bag/device-bag.model';
import { DeviceBagService } from 'src/device-bag/device-bag.service';
import { Bag } from './bag.model';
import { Device } from 'src/devices/devices.model';

@Injectable()
export class BagService {
  constructor(
    @InjectModel(Bag) private bagRepository: typeof Bag,
    private deviceBagService: DeviceBagService,
  ) {}

  async getDevicesInBag(bagId: number) {
    try {
      const bag = await this.bagRepository.findByPk(bagId, {
        include: [
          {
            model: DeviceBag,
            as: 'devices',
            include: [{ model: Device, as: 'device' }],
          },
        ],
      });

      return bag;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async createBag(userId: number) {
    try {
      const bag = await this.bagRepository.create({ userId });

      return bag;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async deleteBag(id: number) {
    try {
      const bag = await this.bagRepository.destroy({ where: { id } });

      return bag > 0;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async addToBag(dto: AddDeviceToBagDto) {
    try {
      const { bagId, deviceId } = dto;

      const deviceInBag = await this.deviceBagService.addToBag(bagId, deviceId);

      return deviceInBag;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async removeFromBag(productId: number, bagId: number) {
    try {
      const isRemoved = await this.deviceBagService.removeFromBag(
        productId,
        bagId,
      );

      if (!isRemoved) {
        throw new HttpException(
          'Device was not removed from bag',
          HttpStatus.BAD_REQUEST,
        );
      }

      return { message: 'Device successfully removed from bag' };
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }
}
