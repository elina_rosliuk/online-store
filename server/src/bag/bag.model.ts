import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { DeviceBag } from 'src/device-bag/device-bag.model';
import { User } from 'src/users/users.model';

interface BagCreationAttrs {
  userId: number;
}

@Table({ tableName: 'bags' })
export class Bag extends Model<Bag, BagCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
  })
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @HasMany(() => DeviceBag)
  devices: DeviceBag[];
}
