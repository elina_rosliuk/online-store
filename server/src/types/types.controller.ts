import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateTypeDto } from './dto/create-type.dto';
import { Type } from './types.model';
import { TypesService } from './types.service';

@ApiTags('Device Types')
@Controller('types')
export class TypesController {
  constructor(private typesService: TypesService) {}

  @ApiOperation({ summary: 'Create Type' })
  @ApiResponse({ status: 200, type: Type })
  @Post()
  create(@Body() typeDto: CreateTypeDto) {
    return this.typesService.createType(typeDto);
  }

  @ApiOperation({ summary: 'Get All Types' })
  @ApiResponse({ status: 200, type: [Type] })
  @Get()
  getAll() {
    return this.typesService.getAllTypes();
  }

  @ApiOperation({ summary: 'Get Type By Id' })
  @ApiResponse({ status: 200, type: Type })
  @Get(':id')
  get(@Param('id') id: string) {
    return this.typesService.getTypeById(id);
  }
}
