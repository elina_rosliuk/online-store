import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { BrandsTypes } from 'src/brands/brand-types.model';
import { Device } from 'src/devices/devices.model';
import { TypesController } from './types.controller';
import { Type } from './types.model';
import { TypesService } from './types.service';

@Module({
  controllers: [TypesController],
  providers: [TypesService],
  imports: [SequelizeModule.forFeature([Type, Device, BrandsTypes])],
  exports: [TypesService],
})
export class TypesModule {}
