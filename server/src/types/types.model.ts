import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  HasMany,
  BelongsToMany,
} from 'sequelize-typescript';
import { BrandsTypes } from 'src/brands/brand-types.model';
import { Brand } from 'src/brands/brands.model';
import { Device } from 'src/devices/devices.model';

interface TypeCreationAttrs {
  title: string;
}

@Table({ tableName: 'types' })
export class Type extends Model<Type, TypeCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'Smartphones' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  title: string;

  @HasMany(() => Device)
  devices: Device[];

  @BelongsToMany(() => Brand, () => BrandsTypes)
  brands: Brand[];
}
