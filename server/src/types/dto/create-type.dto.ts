import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateTypeDto {
  @ApiProperty({ example: 'Smartphone' })
  @IsString({ message: 'Should be a string' })
  @IsNotEmpty({ message: 'The field is required' })
  readonly title: string;
}
