import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateTypeDto } from './dto/create-type.dto';
import { Type } from './types.model';

@Injectable()
export class TypesService {
  constructor(@InjectModel(Type) private typesRepository: typeof Type) {}

  async createType(typeDto: CreateTypeDto) {
    const existingType = await this.getTypeByName(typeDto.title);

    // TODO: Validate for uniqueness on the stage of validation
    if (existingType) {
      throw new HttpException(
        'This device type already exists',
        HttpStatus.BAD_REQUEST,
      );
    }

    const type = await this.typesRepository.create(typeDto);
    return type;
  }

  async getAllTypes() {
    try {
      const types = await this.typesRepository.findAndCountAll();

      return types;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async getTypeById(id: string | number) {
    try {
      const type = await this.typesRepository.findByPk(id);

      if (!type) {
        throw new HttpException('Type was not found', HttpStatus.BAD_REQUEST);
      }

      return type;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  private async getTypeByName(title: string) {
    const type = await this.typesRepository.findOne({ where: { title } });

    return type;
  }
}
