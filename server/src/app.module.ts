import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { DevicesModule } from './devices/devices.module';
import { TypesModule } from './types/types.module';
import { BrandsModule } from './brands/brands.module';
import { User } from './users/users.model';
import { Brand } from './brands/brands.model';
import { Device } from './devices/devices.model';
import { Type } from './types/types.model';
import { BrandsTypes } from './brands/brand-types.model';
import { FilesModule } from './files/files.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { RatingModule } from './rating/rating.module';
import * as path from 'path';
import { Rating } from './rating/rating.model';
import { BagModule } from './bag/bag.module';
import { Bag } from './bag/bag.model';
import { DeviceBagModule } from './device-bag/device-bag.module';
import { DeviceBag } from './device-bag/device-bag.model';
import { TokenModule } from './token/token.module';
import { Token } from './token/token.model';

@Module({
  controllers: [],
  providers: [],
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      models: [
        User,
        Brand,
        Device,
        Type,
        BrandsTypes,
        Rating,
        Bag,
        DeviceBag,
        Token,
      ],
      autoLoadModels: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: path.resolve(__dirname, 'static'),
      exclude: ['/api*'],
    }),
    UsersModule,
    AuthModule,
    DevicesModule,
    TypesModule,
    BrandsModule,
    FilesModule,
    RatingModule,
    BagModule,
    DeviceBagModule,
    TokenModule,
  ],
})
export class AppModule {}
