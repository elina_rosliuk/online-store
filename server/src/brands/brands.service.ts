import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Device } from 'src/devices/devices.model';
import { Brand } from './brands.model';
import { CreateBrandDto } from './dto/create-brand.dto';

@Injectable()
export class BrandsService {
  constructor(@InjectModel(Brand) private brandRepository: typeof Brand) {}

  async createBrand(brandDto: CreateBrandDto) {
    const existingBrand = await this.getBrandByName(brandDto.title);

    // TODO: Validate for uniqueness on the stage of validation
    if (existingBrand) {
      throw new HttpException(
        'This brand already exists',
        HttpStatus.BAD_REQUEST,
      );
    }

    const brand = await this.brandRepository.create(brandDto);
    return brand;
  }

  async getAllBrands() {
    try {
      const brands = await this.brandRepository.findAndCountAll();

      return brands;
    } catch (err) {
      throw new HttpException('Client error', HttpStatus.BAD_REQUEST);
    }
  }

  async getBrandById(id: string | number) {
    try {
      const brand = await this.brandRepository.findByPk(id);

      if (!brand) {
        throw new HttpException('Brand was not found', HttpStatus.BAD_REQUEST);
      }

      return brand;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  private async getBrandByName(title: string) {
    const brand = await this.brandRepository.findOne({ where: { title } });

    return brand;
  }
}
