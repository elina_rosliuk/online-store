import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  ForeignKey,
} from 'sequelize-typescript';
import { Type } from 'src/types/types.model';
import { Brand } from './brands.model';

@Table({ tableName: 'brands_types', createdAt: false, updatedAt: false })
export class BrandsTypes extends Model<BrandsTypes> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Type)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  typeId: number;

  @ApiProperty({ example: 1 })
  @ForeignKey(() => Brand)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  brandId: number;
}
