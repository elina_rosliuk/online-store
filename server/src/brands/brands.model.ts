import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  DataType,
  Table,
  Model,
  HasMany,
  BelongsToMany,
} from 'sequelize-typescript';
import { Device } from 'src/devices/devices.model';
import { Type } from 'src/types/types.model';
import { BrandsTypes } from './brand-types.model';

interface BrandsCreationAttrs {
  title: string;
}

@Table({ tableName: 'brands' })
export class Brand extends Model<Brand, BrandsCreationAttrs> {
  @ApiProperty({ example: 1 })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    primaryKey: true,
    autoIncrement: true,
  })
  id: number;

  @ApiProperty({ example: 'Apple' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  title: string;

  @HasMany(() => Device)
  devices: Device[];

  @BelongsToMany(() => Type, () => BrandsTypes)
  types: Type[];
}
