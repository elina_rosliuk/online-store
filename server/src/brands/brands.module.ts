import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Device } from 'src/devices/devices.model';
import { BrandsTypes } from './brand-types.model';
import { BrandsController } from './brands.controller';
import { Brand } from './brands.model';
import { BrandsService } from './brands.service';

@Module({
  controllers: [BrandsController],
  providers: [BrandsService],
  imports: [SequelizeModule.forFeature([Brand, Device, BrandsTypes])],
  exports: [BrandsService],
})
export class BrandsModule {}
