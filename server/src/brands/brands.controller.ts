import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Brand } from './brands.model';
import { BrandsService } from './brands.service';
import { QueryBrandDto } from './dto/brand-query.dto';
import { CreateBrandDto } from './dto/create-brand.dto';

@ApiTags('Brands')
@Controller('brands')
export class BrandsController {
  constructor(private brandsService: BrandsService) {}

  @ApiOperation({ summary: 'Create Brand' })
  @ApiResponse({ status: 200, type: Brand })
  @Post()
  create(@Body() brandDto: CreateBrandDto) {
    return this.brandsService.createBrand(brandDto);
  }

  @ApiOperation({ summary: 'Get All Brands' })
  @ApiResponse({ status: 200, type: [Brand] })
  @Get()
  getAll() {
    return this.brandsService.getAllBrands();
  }

  @ApiOperation({ summary: 'Get Brand By Id' })
  @ApiResponse({ status: 200, type: Brand })
  @Get(':id')
  get(@Param('id') id: string) {
    return this.brandsService.getBrandById(id);
  }
}
