import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class QueryBrandDto {
  @ApiProperty({ example: 'A' })
  @IsOptional()
  @IsString({ message: 'Should be a string' })
  readonly substring: string;

  @ApiProperty({ example: 5 })
  @IsOptional()
  @IsNumber({}, { message: 'Should be a number' })
  readonly limit: number;
}
