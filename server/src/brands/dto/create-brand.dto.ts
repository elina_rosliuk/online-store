import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateBrandDto {
  @ApiProperty({ example: 'Apple' })
  @IsString({ message: 'Should be a string' })
  @IsNotEmpty({ message: 'The field is required' })
  readonly title: string;
}
