import { Container } from '@mui/material';
import React from 'react';
import { Navbar } from '../../components/Navbar/Navbar';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { UserRole } from '../../types/userReducers';
import styles from './MainLayout.module.scss';

const navbarData = [
  { title: 'Go to products', href: '/devices' },
  {
    icon: { name: 'ShoppingCartIcon' },
    href: '/cart',
    label: 'Shopping Cart',
    isAuth: true,
  },
];

const navbarAdminData = [
  { title: 'Go to products', href: '/devices' },
  { title: 'Manage', href: '/manage/products' },
  {
    icon: { name: 'ShoppingCartIcon' },
    href: '/cart',
    label: 'Shopping Cart',
    isAuth: true,
  },
];

const MainLayout = ({ children }) => {
  const { role } = useTypedSelector((state) => state.user);
  const user = useTypedSelector((state) => state.user);

  const isAdmin = role === UserRole.ADMIN;

  return (
    <div className={styles.mainContainer}>
      {user.isLoading && <p>Loading...</p>}
      {!user.isLoading && (
        <>
          <Navbar data={isAdmin ? navbarAdminData : navbarData} />
          <Container>{children}</Container>
        </>
      )}
    </div>
  );
};

export default MainLayout;
