import { Box, Tab, Tabs } from '@mui/material';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Forbidden } from '../../components/shared/Forbidden/Forbidden';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { UserRole } from '../../types/userReducers';
import MainLayout from '../MainLayout/MainLayout';

const data = [
  { label: 'Products', href: '/manage/products' },
  { label: 'Users', href: '/manage/users' },
];

export default function ManagerLayout({ children }) {
  const router = useRouter();
  const { isAuth, role } = useTypedSelector((state) => state.user);

  if (!isAuth || role !== UserRole.ADMIN) {
    return (
      <MainLayout>
        <Forbidden />
      </MainLayout>
    );
  }

  const getCurrentIndex = (data): number => {
    const item = data.find((item) => router.pathname.includes(item.href));

    return data.indexOf(item);
  };

  return (
    <MainLayout>
      <Box sx={{ width: '100%' }}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs value={getCurrentIndex(data)} aria-label="basic tabs example">
            {data &&
              data.map((item) => (
                <Tab
                  key={item.href}
                  label={item.label}
                  onClick={() => router.push(item.href)}
                />
              ))}
          </Tabs>
        </Box>
        <div>{children}</div>
      </Box>
    </MainLayout>
  );
}
