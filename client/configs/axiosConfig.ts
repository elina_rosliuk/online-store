import axios from 'axios';
import { checkTokenExp } from '../context/authContext';

const instance = axios.create({
  baseURL: 'http://localhost:8000/api/',
  headers: { 'Content-Type': 'application/json' },
  withCredentials: true,
});

instance.interceptors.response.use(
  (config) => {
    return config;
  },
  async (error) => {
    const originalRequest = error.config;
    if (
      error.response.status == 401 &&
      error.config &&
      !originalRequest._isRetry
    ) {
      originalRequest._isRetry = true;

      try {
        if (!checkTokenExp()) {
          await axios.get('http://localhost:8000/api/token/refresh', {
            withCredentials: true,
          });
          originalRequest._isRetry = false;
          return instance.request(originalRequest);
        }
      } catch (err) {
        console.log('NOT AUTHORIZED');
      }
    }
    throw error;
  }
);

export default instance;
