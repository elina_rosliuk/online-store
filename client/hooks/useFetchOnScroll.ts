import { useEffect, useState } from 'react';

const DISTANCE_FOR_FETCH = 50;

export const useFetchOnScroll = ({
  dispatch,
  isFetching,
  fetchProductsFunc,
  productsLength,
  totalCount,
  setFetchingFunc,
  query = null,
}) => {
  const [page, setPage] = useState(1);
  const [searchValue, setSearchValue] = useState('');
  const [filterValue, setFilterValue] = useState('all');

  useEffect(() => {
    if (isFetching && productsLength) {
      const newPage = page + 1;
      dispatch(
        fetchProductsFunc({
          page: newPage,
          search: searchValue,
          query,
          filter: filterValue,
        })
      );
      setPage(newPage);
    }
    if (!isFetching && !productsLength) {
      dispatch(fetchProductsFunc({ page: 1 }));
    }
  }, [isFetching]);

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler);
    return function () {
      document.removeEventListener('scroll', scrollHandler);
    };
  }, [productsLength, totalCount]);

  function scrollHandler(e: Event) {
    const documentTarget = (e.target as Document).documentElement;
    if (
      documentTarget.scrollHeight -
        (documentTarget.scrollTop + window.innerHeight) <
        DISTANCE_FOR_FETCH &&
      productsLength < totalCount
    ) {
      dispatch(setFetchingFunc(true));
    }
  }

  const handleSearch = (searchValue: string) => {
    setPage(1);
    dispatch(fetchProductsFunc({ page: 1, search: searchValue, query }));
  };

  const handleFilter = (filterValue: string) => {
    setPage(1);
    dispatch(fetchProductsFunc({ page: 1, filter: filterValue, query }));
  };

  const setInitialResults = () => {
    setPage(1);
    dispatch(fetchProductsFunc({ page: 1, query }));
  };

  return {
    page,
    setPage,
    handleSearch,
    setInitialResults,
    setSearchValue,
    setFilterValue,
    filterValue,
    handleFilter,
    searchValue,
  };
};
