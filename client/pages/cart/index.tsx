import { NextPage } from 'next';
import Link from 'next/link';
import { BagList } from '../../components/BagList/BagList';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import MainLayout from '../../layouts/MainLayout/MainLayout';
import {
  fetchBagContent,
  setBagLoading,
} from '../../store/action-creators/bagActions';
import { RootState } from '../../store/reducers';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

const CartPage: NextPage<RootState> = () => {
  const dispatch = useDispatch();
  const { bag, loading, totalCount } = useTypedSelector((state) => state.bag);
  const { isAuth, bagId } = useTypedSelector((state) => state.user);

  useEffect(() => {
    dispatch(setBagLoading(true));
    bagId && dispatch(fetchBagContent(bagId));
  }, [bagId]);

  const preparedBagData =
    bag?.length &&
    bag.map(({ bagId, count, deviceId, device }) => ({
      bagId,
      count,
      id: deviceId,
      title: device?.title,
      price: device?.price,
      image: device?.image,
    }));

  return (
    <MainLayout>
      <h2>Shopping Bag</h2>
      {loading && !bag?.length && <p>Loading...</p>}
      {!loading &&
        (bagId && isAuth ? (
          <BagList data={preparedBagData} numOfItems={totalCount} />
        ) : (
          <p>
            <Link href="/auth/login">Log in</Link> to add products to bag
          </p>
        ))}
    </MainLayout>
  );
};

export default CartPage;
