import React, { FC } from 'react';
import { AppProps } from 'next/app';
import { wrapper } from '../store/index';
import '../styles/globals.scss';
import AuthState from '../context/authContext';

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => (
  <AuthState>
    <Component {...pageProps} />
  </AuthState>
);

export default wrapper.withRedux(WrappedApp);
