import { MenuItem, Select } from '@mui/material';
import { NextPage } from 'next';
import { useDispatch } from 'react-redux';
import { ManageUsersList } from '../../components/ManageUsersList/ManageUsersList';
import { useFetchOnScroll } from '../../hooks/useFetchOnScroll';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import ManagerLayout from '../../layouts/ManagerLayout/ManagerLayout';
import {
  fetchUsersList,
  setLoadingUsersList,
} from '../../store/action-creators/usersListActions';
import { RootState } from '../../store/reducers';
import { UserRole } from '../../types/userReducers';
import { UserRoleFilterType } from '../../types/usersListReducers';

const ManageUsersPage: NextPage<RootState> = () => {
  const {
    users,
    error: apiError,
    loading,
    totalCount,
  } = useTypedSelector((state) => state.usersList);
  const dispatch = useDispatch();
  const {
    setPage,
    setFilterValue,
    filterValue,
    handleFilter: getFilteredUsers,
  } = useFetchOnScroll({
    dispatch,
    isFetching: loading,
    totalCount,
    productsLength: users?.length,
    fetchProductsFunc: fetchUsersList,
    setFetchingFunc: setLoadingUsersList,
  });

  const filterValues = [...Object.values(UserRole), 'all'];

  const handleFilter = (e) => {
    const newFilter = e.target.value;
    setFilterValue(e.target.value as UserRoleFilterType);
    setPage(1);
    getFilteredUsers(newFilter as UserRoleFilterType);
  };

  return (
    <ManagerLayout>
      <h2>Users List:</h2>
      <Select value={filterValue} onChange={handleFilter}>
        {filterValues.map((value) => (
          <MenuItem key={value} value={value}>
            {value}
          </MenuItem>
        ))}
      </Select>
      {apiError && <p>{apiError}</p>}
      <ManageUsersList setPage={setPage} data={users} />
    </ManagerLayout>
  );
};

export default ManageUsersPage;
