import { Button } from '@mui/material';
import { useState } from 'react';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import ManagerLayout from '../../layouts/ManagerLayout/ManagerLayout';
import { NextThunkDispatch, wrapper } from '../../store';
import {
  fetchProducts,
  fetchTypes,
  setFetching,
} from '../../store/action-creators/productsActions';
import axios from '../../configs/axiosConfig';
import { ProductModal } from '../../components/ProductModal/ProductModal';
import Portal from '../../components/shared/Portal/Portal';
import { useFetchOnScroll } from '../../hooks/useFetchOnScroll';
import { useDispatch } from 'react-redux';
import { ManageProductList } from '../../components/ManageProductList/ManageProductList';
import { ErrorMessage } from '../../components/shared/ErrorMessage/ErrorMessage';

const ADD_MODAL_ID = 'add-product-modal';

const ManageProductsPage = ({ brands }) => {
  const { products, isFetching, totalCount, types } = useTypedSelector(
    (state) => state.products
  );
  const [openProductModal, setOpenProductModal] = useState<string | null>(null);
  const dispatch = useDispatch();
  const [error, setError] = useState<string | null>(null);
  const { setPage } = useFetchOnScroll({
    dispatch,
    isFetching,
    totalCount,
    productsLength: products.length,
    fetchProductsFunc: fetchProducts,
    setFetchingFunc: setFetching,
  });

  if (error) {
    <ManagerLayout>
      <p>{error}</p>
    </ManagerLayout>;
  }

  const setTemporalError = (message: string) => {
    setError(message);
    error && setTimeout(() => setError(null), 5000);
  };

  const postProduct = async (data) => {
    try {
      const formData = new FormData();
      Object.keys(data).forEach((item) => {
        formData.set(item, data[item]);
      });
      await axios.post('devices', formData, {
        headers: {
          'content-type': 'multipart/form-data',
        },
      });
    } catch (err) {
      const errorRes = err.response;
      if (errorRes.status >= 400 && errorRes.status < 500) {
        setTemporalError(errorRes?.data.message);
      } else {
        setTemporalError('Error');
      }
      console.log(errorRes);
    }
  };

  return (
    <ManagerLayout>
      <Button
        disabled={openProductModal === ADD_MODAL_ID}
        onClick={() => setOpenProductModal(ADD_MODAL_ID)}
      >
        Add New Product
      </Button>
      {openProductModal === ADD_MODAL_ID && (
        <Portal>
          <ProductModal
            id={ADD_MODAL_ID}
            brands={brands}
            types={types}
            setProductModalOpen={setOpenProductModal}
            sendRequest={postProduct}
            actionLabel="Add Product"
            setPage={setPage}
          />
        </Portal>
      )}
      <ManageProductList
        products={products}
        types={types}
        brands={brands}
        setOpenProductModal={setOpenProductModal}
        openProductModal={openProductModal}
        setPage={setPage}
      />
      <ErrorMessage error={error} />
    </ManagerLayout>
  );
};

export default ManageProductsPage;

export const getStaticProps = wrapper.getStaticProps((store) => async (ctx) => {
  try {
    const dispatch = store.dispatch as NextThunkDispatch;
    await dispatch(fetchProducts());
    const { types } = store.getState().products;
    !types && !types.length && (await dispatch(fetchTypes()));

    const brands = await axios.get('brands');

    return {
      props: {
        brands: brands.data,
      },
      revalidate: 120,
    };
  } catch (err) {
    console.log(err);
  }
});
