import MainLayout from '../layouts/MainLayout/MainLayout';
import { NextThunkDispatch, wrapper } from '../store';
import { fetchTypes } from '../store/action-creators/productsActions';

export default function Home() {
  return (
    <MainLayout>
      <h1>Welcome to e-store!</h1>
    </MainLayout>
  );
}

export const getStaticProps = wrapper.getStaticProps((store) => async (ctx) => {
  try {
    const dispatch = store.dispatch as NextThunkDispatch;
    const { types } = store.getState().products;
    !types && !types.length && (await dispatch(fetchTypes()));

    return {
      props: {},
      revalidate: 300,
    };
  } catch (err) {
    console.log(err);
  }
});
