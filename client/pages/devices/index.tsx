import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ProductsList } from '../../components/ProductList/ProductList';
import { Search } from '../../components/shared/Search/Search';
import { useFetchOnScroll } from '../../hooks/useFetchOnScroll';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import MainLayout from '../../layouts/MainLayout/MainLayout';
import { NextThunkDispatch, wrapper } from '../../store';
import {
  fetchProducts,
  setFetching,
} from '../../store/action-creators/productsActions';

const DevicesPage = ({ query }) => {
  const router = useRouter();
  const {
    products,
    error,
    isFetching,
    totalCount,
    currentProductType: currentType,
  } = useTypedSelector((state) => state.products);
  const dispatch = useDispatch();
  const {
    setPage,
    setInitialResults,
    handleSearch,
    setSearchValue,
    searchValue,
  } = useFetchOnScroll({
    dispatch,
    isFetching,
    totalCount,
    productsLength: products.length,
    fetchProductsFunc: fetchProducts,
    setFetchingFunc: setFetching,
    query,
  });

  useEffect(() => {
    setPage(1);
    setSearchValue('');
  }, [router.asPath]);

  if (error) {
    alert(error);
  }

  return (
    <MainLayout>
      <Search
        label="Search"
        id="search-products-field"
        handleChange={handleSearch}
        value={searchValue}
        setSearchValue={setSearchValue}
        setInitialResults={setInitialResults}
      />
      <h2>{currentType ? `${currentType.title} List:` : 'Products List:'}</h2>
      <ProductsList data={products} />
    </MainLayout>
  );
};

export default DevicesPage;

export const getServerSideProps = wrapper.getServerSideProps(
  (store) => async (ctx) => {
    try {
      const dispatch = store.dispatch as NextThunkDispatch;
      await dispatch(fetchProducts({ query: ctx.query }));

      return {
        props: {
          query: ctx.query,
        },
      };
    } catch (err) {
      console.log(err);
    }
  }
);
