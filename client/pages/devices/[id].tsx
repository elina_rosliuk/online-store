import { useTypedSelector } from '../../hooks/useTypedSelector';
import { Product } from '../../components/Product/Product';
import MainLayout from '../../layouts/MainLayout/MainLayout';
import axios from '../../configs/axiosConfig';
import { useState } from 'react';
import { wrapper } from '../../store';

const ProductPage = ({ product, brand, type, error }) => {
  const { bagId } = useTypedSelector((state) => state.user);
  const { id: userId, isAuth } = useTypedSelector((state) => state.user);
  const [productData, setProductData] = useState(product);

  if (error) {
    return <MainLayout>{error}</MainLayout>;
  }

  const updateProduct = async () => {
    try {
      const response = await axios.get('devices/' + product.id);

      setProductData(response.data);
    } catch (err) {
      console.log(err.response);
    }
  };

  return (
    <MainLayout>
      <Product
        {...productData}
        updateProduct={updateProduct}
        brand={brand}
        type={type}
        bagId={bagId}
        isAuth={isAuth}
        userId={userId}
      />
    </MainLayout>
  );
};

export default ProductPage;

export const getServerSideProps = wrapper.getServerSideProps(
  (store) => async (ctx) => {
    try {
      const response = await axios.get('devices/' + ctx.params?.id);

      const { brandId, typeId } = response.data;
      const brand = await axios.get('brands/' + brandId);
      const type = await axios.get('types/' + typeId);

      return {
        props: {
          product: response.data,
          brand: brand.data,
          type: type.data,
          error: null,
        },
      };
    } catch (err) {
      console.log(err.data);
      return {
        props: {
          product: null,
          brand: null,
          type: null,
          error: 'Error while getting product',
        },
      };
    }
  }
);
