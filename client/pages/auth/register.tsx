import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { AuthForm } from '../../components/Auth/AuthForm';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { registerUser } from '../../store/action-creators/authActions';
import { useDispatch } from 'react-redux';

const validations = {
  login: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
  password: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
};

const fields = [
  {
    label: 'Login',
    name: 'login',
    id: 'login-field',
    type: 'text',
    initialValue: '',
  },
  {
    label: 'Password',
    name: 'password',
    id: 'password-field',
    type: 'password',
    initialValue: '',
  },
];

const RegisterPage = () => {
  const { isAuth, error } = useTypedSelector((state) => state.user);
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    if (isAuth) {
      router.push('/');
    }
  }, [isAuth]);

  useEffect(() => {
    router.prefetch('/');
  }, []);

  const handleRegister = ({ login, password }) => {
    dispatch(registerUser({ username: login, password }));
  };

  return (
    <div>
      <AuthForm
        fields={fields}
        validations={validations}
        buttonLabel="Register"
        handleAuth={handleRegister}
        authError={error}
        getRedirectMsg={() => (
          <>
            Already have an account? <Link href="/auth/login">Login</Link>
          </>
        )}
      />
    </div>
  );
};

export default RegisterPage;
