import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { AuthForm } from '../../components/Auth/AuthForm';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../store/action-creators/authActions';
import { wrapper } from '../../store';
import { useTypedSelector } from '../../hooks/useTypedSelector';

const validations = {
  login: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
  password: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
};

const fields = [
  {
    label: 'Login',
    name: 'login',
    id: 'login-field',
    type: 'text',
    initialValue: '',
  },
  {
    label: 'Password',
    name: 'password',
    id: 'password-field',
    type: 'password',
    initialValue: '',
  },
];

const LoginPage = () => {
  const { isAuth, error } = useTypedSelector((state) => state.user);
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    if (isAuth) {
      router.push('/');
    }
  }, [isAuth]);

  useEffect(() => {
    router.prefetch('/');
  }, []);

  const handleAuth = ({ login, password }) => {
    dispatch(loginUser({ username: login, password }));
  };

  return (
    <div>
      <AuthForm
        fields={fields}
        validations={validations}
        buttonLabel="Login"
        handleAuth={handleAuth}
        authError={error}
        getRedirectMsg={() => (
          <>
            Don&#39;t have an account yet?{' '}
            <Link href="/auth/register">Register</Link>
          </>
        )}
      />
    </div>
  );
};

export default LoginPage;

export const getStaticProps = wrapper.getStaticProps((store) => async (ctx) => {
  const { isAuth } = store.getState().user;

  if (isAuth) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }
});
