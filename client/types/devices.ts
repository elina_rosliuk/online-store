export interface IDevice {
  id: number;
  title: string;
  description: string;
  price: number;
  quantity: number;
  image: string;
  brandId: number;
  typeId: number;
  rates: any[];
}

export interface IType {
  id: string;
  title: string;
}

export interface IRate {
  deviceId: number;
  userId: number;
  id: number;
  rate: number;
}
