export enum UserRole {
  CUSTOMER = 'customer',
  ADMIN = 'admin',
}

interface IUser {
  id: number;
  username: string;
  role: UserRole;
  isAuth: boolean;
}

export interface UserState {
  id: number;
  username: string;
  role: UserRole;
  isAuth: boolean;
  error: string;
  isLoading: boolean;
  bagId: number;
}

export enum UserActionTypes {
  SET_USER = 'SET_USER',
  SET_ERROR = 'SET_ERROR',
  LOGOUT = 'LOGOUT',
  SET_LOADING = 'SET_LOADING',
  SET_BAG_ID = 'SET_BAG_ID',
}

interface SetUserAction {
  type: UserActionTypes.SET_USER;
  payload: IUser;
}

interface SetBagIdAction {
  type: UserActionTypes.SET_BAG_ID;
  payload: number;
}

interface SetErrorAction {
  type: UserActionTypes.SET_ERROR;
  payload: string;
}

interface LogoutAction {
  type: UserActionTypes.LOGOUT;
}

interface SetLoadingAction {
  type: UserActionTypes.SET_LOADING;
  payload: boolean;
}

export type UserAction =
  | SetUserAction
  | SetErrorAction
  | LogoutAction
  | SetLoadingAction
  | SetBagIdAction;
