import { IDevice, IType } from './devices';

export interface ProductsState {
  products: IDevice[];
  types: IType[];
  totalCount: number;
  isFetching: boolean;
  error: string;
  currentProductType: IType | '';
}

interface SetProductsPayload {
  products: IDevice[];
  count: number;
}

export enum ProductsActionTypes {
  SET_PRODUCTS = 'SET_PRODUCTS',
  SET_TYPES = 'SET_TYPES',
  SET_CURRENT_TYPE = 'SET_CURRENT_TYPE',
  SET_FETCHING_ERROR = 'SET_FETCHING_ERROR',
  SET_FETCHING = 'SET_FETCHING',
  SET_NEW_PRODUCTS = 'SET_NEW_PRODUCTS',
}

interface SetProductsAction {
  type: ProductsActionTypes.SET_PRODUCTS;
  payload: SetProductsPayload;
}

interface SetCurrentTypeAction {
  type: ProductsActionTypes.SET_CURRENT_TYPE;
  payload: IType;
}

interface SetTypesAction {
  type: ProductsActionTypes.SET_TYPES;
  payload: IType[];
}

interface SetNewProductsAction {
  type: ProductsActionTypes.SET_NEW_PRODUCTS;
  payload: SetProductsPayload;
}

interface SetFetchingAction {
  type: ProductsActionTypes.SET_FETCHING;
  payload: boolean;
}

interface ErrorProductsAction {
  type: ProductsActionTypes.SET_FETCHING_ERROR;
  payload: string;
}

export type ProductsAction =
  | SetProductsAction
  | SetNewProductsAction
  | ErrorProductsAction
  | SetFetchingAction
  | SetTypesAction
  | SetCurrentTypeAction;
