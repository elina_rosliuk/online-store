import { UserRole } from './userReducers';

interface IUser {
  id: number;
  username: string;
  role: UserRole;
  bagId: number;
}

export type UserRoleFilterType = UserRole.CUSTOMER | UserRole.ADMIN | 'all';

export interface UsersListState {
  users: IUser[];
  error: string;
  loading: boolean;
  totalCount: number;
}

export enum UsersListActionTypes {
  SET_USERS_LIST = 'SET_USERS_LIST',
  SET_NEW_USERS_LIST = 'SET_NEW_USERS_LIST',
  SET_USERS_LIST_TOTAL_COUNT = 'SET_USERS_LIST_TOTAL_COUNT',
  SET_USERS_LIST_API_ERROR = 'SET_USERS_LIST_API_ERROR',
  SET_USERS_LIST_LOADING = 'SET_USERS_LIST_LOADING',
}

interface SetUsersListAction {
  type: UsersListActionTypes.SET_USERS_LIST;
  payload: IUser[];
}

interface SetNewUsersListAction {
  type: UsersListActionTypes.SET_NEW_USERS_LIST;
  payload: IUser[];
}

interface SetUsersListTotalCountAction {
  type: UsersListActionTypes.SET_USERS_LIST_TOTAL_COUNT;
  payload: number;
}

interface SetUsersListApiErrorAction {
  type: UsersListActionTypes.SET_USERS_LIST_API_ERROR;
  payload: string;
}

interface SetUsersListLoadingAction {
  type: UsersListActionTypes.SET_USERS_LIST_LOADING;
  payload: boolean;
}

export type UsersListAction =
  | SetUsersListAction
  | SetNewUsersListAction
  | SetUsersListTotalCountAction
  | SetUsersListApiErrorAction
  | SetUsersListLoadingAction;
