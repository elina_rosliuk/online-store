import { IDevice } from './devices';

export interface IBagContent {
  id: number;
  bagId: number;
  deviceId: number;
  count: number;
  createdAt: string;
  updatedAt: string;
  device: IDevice;
}

export interface BagState {
  bag: IBagContent[];
  error: string;
  loading: boolean;
  totalCount: number;
}

export enum BagActionTypes {
  SET_BAG_CONTENT = 'SET_BAG_CONTENT',
  SET_FETCHING_ERROR = 'SET_FETCHING_ERROR',
  SET_BAG_LOADING = 'SET_BAG_LOADING',
  SET_BAG_TOTAL_COUNT = 'SET_BAG_TOTAL_COUNT',
  CLEAR_BAG_CONTENT = 'CLEAR_BAG_CONTENT',
}

interface SetBagAction {
  type: BagActionTypes.SET_BAG_CONTENT;
  payload: IBagContent[];
}

interface SetBagTotalCountAction {
  type: BagActionTypes.SET_BAG_TOTAL_COUNT;
  payload: number;
}

interface ClearBagAction {
  type: BagActionTypes.CLEAR_BAG_CONTENT;
}

interface SetErrorAction {
  type: BagActionTypes.SET_FETCHING_ERROR;
  payload: string;
}

interface SetLoadingAction {
  type: BagActionTypes.SET_BAG_LOADING;
  payload: boolean;
}

export type BagsAction =
  | SetBagAction
  | SetErrorAction
  | SetLoadingAction
  | ClearBagAction
  | SetBagTotalCountAction;
