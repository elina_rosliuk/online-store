export interface IProductInList {
  title: string;
  id: number;
  price: number;
  image: string;
  quantity: number;
}
