import { NextThunkDispatch } from '../store';
import { checkAuthAndSetUser } from './checkAuth';

export const handleAuthOnServerSide = (ctx, store) => {
  const dispatch = store.dispatch as NextThunkDispatch;
  const { isAuth } = store.getState().user;

  if (ctx.req) {
    checkAuthAndSetUser(ctx, dispatch, isAuth);
  }
};
