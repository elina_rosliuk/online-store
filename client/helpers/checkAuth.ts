import { logoutUser } from '../store/action-creators/authActions';
import { setUser } from '../store/action-creators/userActions';

export const checkAuthAndSetUser = (ctx, dispatch, isAuth) => {
  const token = ctx.req?.cookies?.JWT_REFRESH_TOKEN;
  const user = ctx.req?.cookies?.user ? JSON.parse(ctx.req.cookies.user) : null;

  if (isAuth && token && user) return;

  if (!isAuth && token && user) {
    dispatch(
      setUser({
        id: user.id,
        role: user.role,
        username: user.username,
        bagId: user.bagId,
      })
    );
  }

  if (isAuth && (!token || !user)) {
    dispatch(logoutUser());
  }
};

export const redirectUnauthentificated = (
  isAuth,
  redirectLink = '/auth/login'
) => {
  if (!isAuth) {
    return {
      redirect: {
        destination: redirectLink,
        permanent: false,
      },
    };
  }
};
