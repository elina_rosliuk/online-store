export const eraseCookie = (name: string) => {
  document.cookie = name + '=; Max-Age=0';
};

export const parseCookie = (str = '') =>
  str
    ? str
        .split(';')
        .map((v) => v.split('='))
        .reduce((acc, v) => {
          acc[decodeURIComponent(v[0].trim())] = JSON.parse(
            decodeURIComponent(v[1].trim())
          );
          return acc;
        }, {})
    : '';

export const getIsExpired = (date: string) => {
  const expDate = new Date(date);
  const currentDate = new Date();

  return expDate < currentDate;
};

export const parseAndGetCookieByName = (name: string) => {
  const cookies =
    parseCookie(typeof document !== 'undefined' && document.cookie) || {};

  return cookies[name];
};
