import React, { createContext, useEffect, useContext } from 'react';
import { useDispatch } from 'react-redux';
import {
  eraseCookie,
  getIsExpired,
  parseAndGetCookieByName,
} from '../helpers/cookieHelpers';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { clearUser } from '../store/action-creators/authActions';
import {
  clearBagContent,
  fetchBagContent,
} from '../store/action-creators/bagActions';
import { loadUser } from '../store/action-creators/userActions';

interface AuthProps {
  children: any;
}

export const AuthContext = createContext({});

export function useAuth() {
  return useContext(AuthContext);
}

export const checkTokenExp = () => {
  const tokenExpirationDate = parseAndGetCookieByName('REFRESH_TOKEN_LIFE_EXP');

  return tokenExpirationDate && getIsExpired(tokenExpirationDate);
};

const clearCommonUserData = (dispatch) => {
  dispatch(clearUser());
  dispatch(clearBagContent());
};

const AuthState = ({ children }: AuthProps) => {
  const { isAuth, bagId } = useTypedSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isAuth && !checkTokenExp()) dispatch(loadUser());
  }, []);

  useEffect(() => {
    if (isAuth && bagId) dispatch(fetchBagContent(bagId));
  }, [isAuth]);

  const tokenExpirationDate = parseAndGetCookieByName('REFRESH_TOKEN_LIFE_EXP');

  if (isAuth && !tokenExpirationDate) {
    clearCommonUserData(dispatch);
  }

  if (checkTokenExp()) {
    clearCommonUserData(dispatch);
    eraseCookie('REFRESH_TOKEN_LIFE_EXP');
  }
  return <AuthContext.Provider value={{}}>{children}</AuthContext.Provider>;
};

export default AuthState;
