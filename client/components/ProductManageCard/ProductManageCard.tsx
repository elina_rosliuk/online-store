import { FC, useState } from 'react';
import { Button, Card } from '@mui/material';
import styles from './ProductManageCard.module.scss';
import { ProductModal } from '../ProductModal/ProductModal';
import Portal from '../shared/Portal/Portal';
import axios from '../../configs/axiosConfig';
import Image from 'next/image';
import { ErrorMessage } from '../shared/ErrorMessage/ErrorMessage';

const PREFIX = 'http://localhost:8000/';

interface IProps {
  product: any;
  brands: any[];
  types: any;
  setOpenProductModal: any;
  openProductModal: string | null;
  setPage: any;
}

export const ProductManageCard: FC<IProps> = ({
  product,
  brands,
  types,
  setOpenProductModal,
  openProductModal,
  setPage,
}) => {
  const [error, setError] = useState<string | null>(null);
  const { id, image, title, price, quantity, brandId, typeId, description } =
    product;

  const setTemporalError = (message: string) => {
    setError(message);
    setTimeout(() => setError(null), 5000);
  };

  const updateProduct = async (data) => {
    try {
      const { image, ...info } = data;

      await axios.put(`devices/${id}`, info);

      if (image) {
        const formData = new FormData();
        formData.set('image', image);
        await axios.patch(`devices/${id}/image`, formData, {
          headers: {
            'content-type': 'multipart/form-data',
          },
        });
      }
    } catch (err) {
      const errorRes = err.response;
      if (errorRes.status >= 400 && errorRes.status < 500) {
        setTemporalError(errorRes?.data.message);
      } else {
        setTemporalError('Error');
      }
      console.log(errorRes);
    }
  };

  return (
    <>
      <ErrorMessage error={error} />
      <Card variant="outlined" className={styles.wrapper}>
        <div>
          <Image
            src={`${PREFIX}${image}`}
            alt={`${title} Picture`}
            width={100}
            height={100}
          />
        </div>
        <div className={styles.contentWrapper}>
          <h3>{title}</h3>
          <p className={styles.text}>Id: {id}</p>
          <p className={styles.text}>Price: ${price}</p>
        </div>
        <div className={styles.actionButtonWrapper}>
          <Button
            className={styles.actionButton}
            onClick={() => {
              setOpenProductModal(id);
            }}
          >
            Update
          </Button>
        </div>
      </Card>
      {openProductModal === id && (
        <Portal>
          <ProductModal
            brands={brands}
            types={types}
            product={{
              id,
              title,
              price,
              quantity,
              brandId,
              typeId,
              description,
            }}
            setProductModalOpen={setOpenProductModal}
            sendRequest={updateProduct}
            actionLabel="Update Product"
            setPage={setPage}
          />
        </Portal>
      )}
    </>
  );
};
