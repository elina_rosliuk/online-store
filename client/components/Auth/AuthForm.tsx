import { ChangeEvent, FC, Fragment } from 'react';
import { Button, TextField } from '@mui/material';
import { useForm } from '../../hooks/useForm';
import styles from './AuthForm.module.scss';

interface IProps {
  fields: any[];
  validations: any;
  getRedirectMsg: any;
  buttonLabel: string;
  handleAuth: any;
  authError?: string;
}

export const AuthForm: FC<IProps> = ({
  fields = [],
  validations = {},
  getRedirectMsg,
  buttonLabel = '',
  authError,
  handleAuth,
}) => {
  const initialValues =
    fields && fields.map((field) => ({ [field.name]: field.initialValue }));
  const {
    data: formData,
    handleSubmit,
    handleChange,
    errors,
  } = useForm({
    initialValues,
    validations,
    onSubmit: handle,
  });

  function handle() {
    return handleAuth({ ...formData });
  }

  const renderFields = (data: any[]) => {
    return (
      data.length &&
      data.map((field) => (
        <Fragment key={field.name}>
          <TextField
            id={field.id}
            label={field.label}
            type={field.type || 'text'}
            variant="standard"
            name={field.name}
            className={styles.field}
            value={formData[field.name]}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              handleChange(field.name)(e)
            }
          />
          <div className={styles.errorWrapper}>
            {errors[field.name] && (
              <p className={styles.error}>{errors[field.name]}</p>
            )}
          </div>
        </Fragment>
      ))
    );
  };

  return (
    <form onSubmit={handleSubmit} className={styles.form}>
      <div className={styles.errorWrapper}>
        {authError && <p className={styles.error}>{authError}</p>}
      </div>
      {renderFields(fields)}
      <Button type="submit" variant="outlined" className={styles.button}>
        {buttonLabel}
      </Button>
      {getRedirectMsg && <p>{getRedirectMsg()}</p>}
    </form>
  );
};
