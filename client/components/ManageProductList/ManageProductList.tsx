import { FC } from 'react';
import styles from './ManageProductList.module.scss';
import { ProductManageCard } from '../ProductManageCard/ProductManageCard';

interface IProps {
  products: any[];
  brands: any[];
  types: any[];
  openProductModal: string | null;
  setOpenProductModal: any;
  setPage: any;
}

export const ManageProductList: FC<IProps> = ({
  products = [],
  brands,
  types,
  openProductModal,
  setOpenProductModal,
  setPage,
}) => {
  return (
    <>
      <h2>Products List:</h2>
      <ul className={styles.list}>
        {products.length &&
          products.map((product) => (
            <ProductManageCard
              key={product.id}
              product={product}
              brands={brands}
              types={types}
              openProductModal={openProductModal}
              setOpenProductModal={setOpenProductModal}
              setPage={setPage}
            />
          ))}
      </ul>
    </>
  );
};
