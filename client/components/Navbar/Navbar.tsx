import React, { FC, useEffect } from 'react';

import { Container } from '@mui/material';
import { CustomLink as Link } from '../shared/Link/Link';
import styles from './Navbar.module.scss';
import { DrawerMenu } from '../DrawerMenu/DrawerMenu';
import { fetchTypes } from '../../store/action-creators/productsActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { useDispatch } from 'react-redux';
import { logoutUser } from '../../store/action-creators/authActions';
import { ShoppingCartButtonIcon } from '../shared/ShoppingCartIcon/ShoppingCartIcon';

interface IMenuitem {
  title?: string;
  href: string;
  icon?: { name: string };
  label?: string;
}

interface NavbarProps {
  data: IMenuitem[];
}

export const Navbar: FC<NavbarProps> = ({ data }) => {
  const { types } = useTypedSelector((state) => state.products);
  const { isAuth } = useTypedSelector((state) => state.user);
  const bag = useTypedSelector((state) => state.bag);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!types || types.length === 0) {
      dispatch(fetchTypes());
    }
  }, [types]);

  const getIcon = (name = '') => {
    switch (name) {
      case 'ShoppingCartIcon':
        return <ShoppingCartButtonIcon count={bag.totalCount} />;
      default:
        return '';
    }
  };

  const renderNavbarKeys = (data) => {
    return data.map(
      (item) =>
        (!item.isAuth || (item.isAuth && isAuth)) && (
          <Link
            className={styles.menuitem}
            label={item.label}
            key={item.title || item?.icon.name}
            href={item.href}
          >
            {item.title ? item.title : ''}
            {item.icon && getIcon(item.icon.name)}
          </Link>
        )
    );
  };

  const handleAuth = () => {
    if (isAuth) {
      dispatch(logoutUser());
    }
  };

  return (
    <Container component="nav" maxWidth="xl" className={styles.navbar}>
      <div className={styles.leftPart}>
        <DrawerMenu types={types} />
        <Link href="/">
          <h1 title="Go to main" className={styles.logo}>
            E-Commerce
          </h1>
        </Link>
      </div>
      <div className={styles.menu}>
        {renderNavbarKeys(data)}
        <Link
          onClick={handleAuth}
          className={styles.menuitem}
          href={isAuth ? '/' : '/auth/login'}
        >
          {isAuth ? 'Logout' : 'Login'}
        </Link>
      </div>
    </Container>
  );
};
