import { FC } from 'react';
import { Button, Card } from '@mui/material';
import { IProductInList } from '../../types/productList';
import styles from './ProductCard.module.scss';
import { useRouter } from 'next/router';
import Image from 'next/image';

const PREFIX = 'http://localhost:8000/';

export const ProductCard: FC<IProductInList> = ({
  title,
  price,
  id,
  image,
  quantity,
}) => {
  const router = useRouter();

  const handleClick = () => {
    router.push(`/devices/${id}`);
  };

  return (
    <Card className={styles.card} variant="outlined">
      <Image
        src={`${PREFIX}${image}`}
        alt={`${title} Picture`}
        width={220}
        height={220}
        onClick={handleClick}
      />
      <div className={styles.titleWrapper}>
        <p className={styles.title} onClick={handleClick} title={title}>
          {title}
        </p>
      </div>
      <div className={styles.buyingPanel}>
        <p className={styles.priceLabel}>${price}</p>
        <Button
          className={styles.addButton}
          variant="outlined"
          onClick={handleClick}
        >
          To product ➤
        </Button>
      </div>
    </Card>
  );
};
