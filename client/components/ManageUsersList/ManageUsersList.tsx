import { FC, useState } from 'react';
import { Container } from '@mui/material';
import { UserCard } from '../UserCard/UserCard';
import { useTypedSelector } from '../../hooks/useTypedSelector';

interface IProps {
  data: any[];
  setPage: (param: number) => void;
}

export const ManageUsersList: FC<IProps> = ({ data, setPage }) => {
  const [isRoleModalOpen, setRoleModalOpen] = useState<false | string>(false);
  const { id: currentUserId } = useTypedSelector((state) => state.user);

  // TODO: add loader
  const filterAndRenderUsers = () => {
    return data.map((user) => (
      <UserCard
        isRoleModalOpen={isRoleModalOpen}
        setRoleModalOpen={setRoleModalOpen}
        setPage={setPage}
        key={user.id}
        user={user}
        currentUserId={currentUserId}
      />
    ));
  };

  return (
    <>
      <Container>
        {data?.length ? <div>{filterAndRenderUsers()}</div> : 'No users found'}
      </Container>
    </>
  );
};
