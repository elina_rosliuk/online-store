import { FC } from 'react';
import { Button, Card } from '@mui/material';
import styles from './BagItem.module.scss';
import axios from '../../configs/axiosConfig';
import { useDispatch } from 'react-redux';
import { fetchBagContent } from '../../store/action-creators/bagActions';
import Image from 'next/image';

const PREFIX = 'http://localhost:8000/';

interface IProps {
  title: string;
  image: string;
  price: number;
  id: number;
  bagId: number;
  count: number;
}

export const BagItem: FC<IProps> = ({
  id,
  title = '',
  image,
  price,
  bagId,
  count = 1,
}) => {
  const dispatch = useDispatch();

  const handleRemove = async () => {
    try {
      await axios.delete(`bag/${bagId}?product=${id}`);
      dispatch(fetchBagContent(bagId));
    } catch (err) {
      console.log(err);
    }
  };

  const handleAdd = async () => {
    try {
      const payload = { deviceId: id, bagId };

      await axios.post('bag', payload);
      dispatch(fetchBagContent(bagId));
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Card variant="outlined" className={styles.wrapper}>
      <div>
        <Image
          src={`${PREFIX}${image}`}
          width={100}
          height={100}
          alt={`${title} Picture`}
        />
      </div>
      <div className={styles.contentWrapper}>
        <h3>{title}</h3>
        <p className={styles.price}>${price}</p>
      </div>
      <div className={styles.addRemoveButtonWrapper}>
        <Button
          variant="outlined"
          title="Add Product"
          onClick={handleAdd}
          className={styles.addRemoveButton}
        >
          +
        </Button>
        <p className={styles.count}>{count}</p>
        <Button
          variant="outlined"
          title="Remove Product"
          onClick={handleRemove}
          className={styles.addRemoveButton}
        >
          -
        </Button>
      </div>
    </Card>
  );
};
