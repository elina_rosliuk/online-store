import { FC, useState } from 'react';
import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { UserRole } from '../../types/userReducers';
import styles from './ChangeRoleModal.module.scss';
import { ModalOverlay } from '../shared/ModalOverlay/ModalOverlay';
import axios from '../../configs/axiosConfig';
import { useDispatch } from 'react-redux';
import { fetchUsersList } from '../../store/action-creators/usersListActions';

interface IProps {
  currentRole: UserRole;
  handleModalOpen: (param: string | false) => void;
  userId: string;
  setPage: (param: number) => void;
}

export const ChangeRoleModal: FC<IProps> = ({
  currentRole,
  handleModalOpen,
  userId,
  setPage,
}) => {
  const [role, setRole] = useState(currentRole);
  const dispatch = useDispatch();

  const handleChange = (event: SelectChangeEvent<string>) => {
    setRole(event.target.value as UserRole);
  };

  const sendRoleChange = async () => {
    if (
      role !== currentRole &&
      Object.values(UserRole).includes(role) &&
      userId
    ) {
      try {
        await axios.patch(`/users/role/${userId}`, { role });
        setPage(1);
        dispatch(fetchUsersList());
      } catch (e) {
        console.log(e.response);
      } finally {
        closeModal();
      }
    }
  };

  const closeModal = () => handleModalOpen(false);

  return (
    <>
      <ModalOverlay handleChange={closeModal} />
      <Container className={styles.wrapper}>
        <p>Change user role to:</p>
        <FormControl className={styles.form}>
          <InputLabel id="role-select-label">Role</InputLabel>
          <Select
            labelId="role-select-label"
            id="role-select"
            value={role}
            label="Role"
            onChange={handleChange}
          >
            {Object.values(UserRole).map((role) => (
              <MenuItem key={role} value={role}>
                {role}
              </MenuItem>
            ))}
          </Select>
          <div className={styles.buttonWrapper}>
            <Button
              className={styles.formButton}
              variant="outlined"
              onClick={sendRoleChange}
            >
              Save
            </Button>
            <Button
              className={styles.formButton}
              variant="outlined"
              onClick={closeModal}
            >
              Cancel
            </Button>
          </div>
        </FormControl>
      </Container>
    </>
  );
};
