import { FC } from 'react';
import { Container } from '@mui/material';
import { IProductInList } from '../../types/productList';
import { ProductCard } from '../ProductCard/ProductCard';
import styles from './ProductsList.module.scss';

interface IProps {
  data: IProductInList[];
}

export const ProductsList: FC<IProps> = ({ data }) => {
  return (
    <Container className={styles.list}>
      {data.length
        ? data.map(({ title, image, id, price, quantity }) => (
            <ProductCard
              key={id}
              id={id}
              title={title}
              quantity={quantity}
              price={price}
              image={image}
            />
          ))
        : 'No products found'}
    </Container>
  );
};
