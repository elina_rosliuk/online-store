import { FC } from 'react';
import styles from './ModalOverlay.module.scss';

interface IProps {
  handleChange?: () => void;
}

export const ModalOverlay: FC<IProps> = ({ handleChange }) => (
  <div className={styles.overlay} onClick={handleChange} />
);
