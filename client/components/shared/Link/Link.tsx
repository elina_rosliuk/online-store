import Link from 'next/link';

export const CustomLink = ({
  href,
  className = '',
  label = '',
  children,
  onClick = null,
}) => (
  <Link href={href}>
    <a className={className} title={label} onClick={onClick}>
      {children}
    </a>
  </Link>
);
