import Link from 'next/link';

export const Forbidden = () => (
  <div>
    <h1>403 Forbidden</h1>
    <p>Make sure that you have relevant rights to visit this page</p>
    <p>
      Go to <Link href="/">the main page</Link>
    </p>
  </div>
);
