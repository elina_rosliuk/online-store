import { FC } from 'react';
import { Button, Container } from '@mui/material';
import styles from './ManageProductModal.module.scss';
import CloseIcon from '@mui/icons-material/Close';
import { ModalOverlay } from '../ModalOverlay/ModalOverlay';

interface IProps {
  actionLabel: string;
  setProductModalOpen: (param: string | false) => void;
  handleSubmit: any;
  children: any;
}

export const ManageProductModal: FC<IProps> = ({
  actionLabel = '',
  setProductModalOpen,
  handleSubmit,
  children,
}) => {
  const handleCloseModal = () => {
    setProductModalOpen && setProductModalOpen(false);
  };

  return (
    <>
      <ModalOverlay handleChange={handleCloseModal} />
      <Container className={styles.wrapper}>
        <Button onClick={handleCloseModal}>
          <CloseIcon />
        </Button>
        <form onSubmit={handleSubmit}>
          <Container className={styles.formWrapper}>{children}</Container>
          <Button type="submit">{actionLabel}</Button>
          <Button onClick={() => setProductModalOpen(null)}>Cancel</Button>
        </form>
      </Container>
    </>
  );
};
