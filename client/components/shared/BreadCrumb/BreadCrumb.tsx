import { Breadcrumbs, Typography } from '@mui/material';
import { CustomLink as Link } from '../Link/Link';

export const BreadCrumb = ({ data }) => {
  return (
    <Breadcrumbs separator="›" aria-label="breadcrumb">
      {data &&
        data.map((item) => {
          return item.href ? (
            <Link key={item.title} href={item.href}>
              {item.title}
            </Link>
          ) : (
            <Typography key={item.title} color="text.primary">
              {item.title}
            </Typography>
          );
        })}
    </Breadcrumbs>
  );
};
