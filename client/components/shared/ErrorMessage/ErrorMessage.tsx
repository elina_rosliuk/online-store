import { Alert } from '@mui/material';
import { FC } from 'react';
import Portal from '../Portal/Portal';
import styles from './ErrorMessage.module.scss';

interface IProps {
  error?: string;
}

export const ErrorMessage: FC<IProps> = ({ error = '' }) => {
  if (!error) return null;

  return (
    <Portal>
      <div className={styles.alert}>
        <Alert severity="error">{error}</Alert>
      </div>
    </Portal>
  );
};
