import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { FC } from 'react';

interface IProps {
  count?: number;
}

export const ShoppingCartButtonIcon: FC<IProps> = ({ count = 0 }) => {
  return (
    <div>
      <ShoppingCartIcon />
      {count && <span>{count}</span>}
    </div>
  );
};
