import { TextField } from '@mui/material';
import { ChangeEvent, FC, useEffect, useState } from 'react';
import { useDebounce } from '../../../hooks/useDebouce';

interface IProps {
  id: string;
  label: string;
  variant?: 'standard' | 'filled' | 'outlined';
  handleChange?: any;
  setSearchValue?: any;
  setInitialResults?: any;
  value?: string;
}

export const Search: FC<IProps> = ({
  id,
  label,
  variant = 'standard',
  value,
  setSearchValue,
  handleChange,
}) => {
  const [hasAlreadySearched, setHasAlreadySearched] = useState(false);
  const debouncedSearchTerm = useDebounce(value, 500);

  useEffect(() => {
    if (hasAlreadySearched) {
      handleChange && handleChange(debouncedSearchTerm);
    }
  }, [debouncedSearchTerm]);

  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setHasAlreadySearched(true);
    setSearchValue(e.target.value);
  };

  return (
    <TextField
      value={value}
      onChange={handleSearch}
      type="search"
      id={id}
      label={label}
      variant={variant}
    />
  );
};
