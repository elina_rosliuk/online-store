import { ChangeEvent, FC, Fragment, useState } from 'react';
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import styles from './ProductModal.module.scss';
import { useDispatch } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { fetchProducts } from '../../store/action-creators/productsActions';
import { ManageProductModal } from '../shared/ProductModal/ManageProductModal';

const MAX_ALLOWED_IMG_SIZE = 5000000;
const IMG_PATTERN = /\.(jpe?g|png|bmp)$/;

const validations = {
  title: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
  description: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
  price: {
    required: {
      value: true,
      message: 'This field is required',
    },
    pattern: {
      value: '^[0-9]+$',
      message: 'Only numbers are allowed',
    },
  },
  quantity: {
    required: {
      value: true,
      message: 'This field is required',
    },
    pattern: {
      value: '^[0-9]+$',
      message: 'Only numbers are allowed',
    },
  },
  typeId: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
  brandId: {
    required: {
      value: true,
      message: 'This field is required',
    },
  },
};

const inputsData = [
  {
    placeholder: 'Title',
    name: 'title',
    type: 'text',
  },
  {
    placeholder: 'Description',
    name: 'description',
    type: 'text',
  },
  {
    placeholder: 'Price',
    name: 'price',
    type: 'number',
    inputProps: { inputMode: 'numeric', pattern: '[0-9]*' },
  },
  {
    placeholder: 'Quantity',
    name: 'quantity',
    type: 'number',
    inputProps: { inputMode: 'numeric', pattern: '[0-9]*' },
  },
  {
    name: 'typeId',
    label: 'Type',
    key: 'types',
    type: 'select',
  },
  {
    name: 'brandId',
    label: 'Brand',
    key: 'brands',
    type: 'select',
  },
];

interface IProps {
  setProductModalOpen: (param: string | false) => void;
  brands: any;
  types: any;
  product: any;
  sendRequest: any;
  actionLabel: string;
  setPage: (param: number) => void;
}

export const ProductModal: FC<IProps> = ({
  setProductModalOpen,
  brands,
  types,
  product,
  sendRequest,
  actionLabel = '',
  setPage,
}) => {
  const dispatch = useDispatch();
  const { handleSubmit, handleChange, data, errors } = useForm({
    initialValues: product,
    validations,
    onSubmit: handleSendProduct,
  });
  const [image, setImage] = useState(null);
  const [imageError, setImageError] = useState(null);

  if (!brands || !types) {
    return <p>Error. Reload the page</p>;
  }

  const selectItemsData = {
    brands: brands.rows,
    types,
  };

  const validateImage = (img: File) => {
    if (imageError) {
      setImageError(null);
    }

    if (!img) {
      setImageError('Files was not uploaded.');
      return false;
    }

    if (!new RegExp(IMG_PATTERN).test(img.name)) {
      setImageError(
        'Files only with jpg, jpeg, bmp and png extensions are allowed'
      );
      return false;
    }

    if (img.size > MAX_ALLOWED_IMG_SIZE) {
      setImageError('Files only up to 5 MB are allowed');
      return false;
    }

    return true;
  };

  const handleImageUpload = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target?.files[0];

    if (file) {
      validateImage(file) && setImage(file);
    }
  };

  async function handleSendProduct() {
    if (image) {
      data.image = image;
    }

    await sendRequest(data);
    setProductModalOpen(null);
    setPage(1);
    await dispatch(fetchProducts());
  }

  const renderSelectField = ({ label, key, name, selectItems, finalData }) => {
    const labelId = `${key}-select-label`;
    const selectId = `${key}-select`;

    return (
      <FormControl className={styles.formControl} fullWidth>
        <InputLabel id={labelId}>{label}</InputLabel>
        <Select
          labelId={labelId}
          id={selectId}
          value={finalData[name] || ''}
          onChange={handleChange(name)}
          name={name}
          label={label}
          className={styles.block}
        >
          {selectItems[key] &&
            selectItems[key].map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.title}
              </MenuItem>
            ))}
        </Select>
        {errors[name] && (
          <FormHelperText className={styles.block}>
            {errors[name]}
          </FormHelperText>
        )}
      </FormControl>
    );
  };

  const renderTextField = ({
    name,
    type = 'text',
    placeholder = '',
    inputProps = {},
  }) => {
    return (
      <FormControl className={styles.formControl} fullWidth key={name}>
        <TextField
          onChange={handleChange(name)}
          type={type}
          value={data[name] || ''}
          name={name}
          placeholder={placeholder}
          inputProps={inputProps}
        />
        {errors[name] && <FormHelperText>{errors[name]}</FormHelperText>}
      </FormControl>
    );
  };

  return (
    <ManageProductModal
      actionLabel={actionLabel}
      setProductModalOpen={setProductModalOpen}
      handleSubmit={handleSubmit}
    >
      <div>
        {inputsData.map((input) => {
          switch (input.type) {
            case 'text':
            case 'number':
              return (
                <Fragment key={input.name}>{renderTextField(input)}</Fragment>
              );
            case 'select':
              return (
                <Fragment key={input.name}>
                  {renderSelectField({
                    label: input.label,
                    key: input.key,
                    name: input.name,
                    finalData: data,
                    selectItems: selectItemsData,
                  })}
                </Fragment>
              );
          }
        })}
      </div>
      <div>
        <FormControl className={styles.formControl} fullWidth>
          <TextField type="file" name="image" onChange={handleImageUpload} />
          {imageError && <FormHelperText>{imageError}</FormHelperText>}
        </FormControl>
      </div>
    </ManageProductModal>
  );
};
