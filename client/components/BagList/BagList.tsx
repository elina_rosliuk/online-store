import { FC } from 'react';
import { Container } from '@mui/material';
import styles from './BagList.module.scss';
import { BagItem } from '../BagItem/BagItem';

interface IProps {
  data: any[];
  numOfItems: number;
}

export const BagList: FC<IProps> = ({ data = [], numOfItems = 0 }) => {
  const itemLabel = numOfItems === 1 ? 'item' : 'items';
  const subTitle = `${numOfItems} ${itemLabel} in the bag`;

  return (
    <Container className={styles.wrapper}>
      <h3 className={styles.subtitle}>{subTitle}</h3>
      {numOfItems
        ? data.map((product) => <BagItem key={product.id} {...product} />)
        : 'Your shopping bag is empty'}
    </Container>
  );
};
