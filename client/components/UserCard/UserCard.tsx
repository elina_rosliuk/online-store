import { FC, useState } from 'react';
import { Button, Card } from '@mui/material';
import styles from './UserCard.module.scss';
import Image from 'next/image';
import { ErrorMessage } from '../shared/ErrorMessage/ErrorMessage';
import Portal from '../shared/Portal/Portal';
import { ChangeRoleModal } from '../ChangeRoleModal/ChangeRoleModal';

interface IProps {
  user: any;
  isRoleModalOpen: false | string;
  setRoleModalOpen: (param: string | false) => void;
  setPage: (param: number) => void;
  currentUserId: number;
}

export const UserCard: FC<IProps> = ({
  user,
  isRoleModalOpen,
  setRoleModalOpen,
  setPage,
  currentUserId,
}) => {
  const [error, setError] = useState<string | null>(null);
  const { id, username, role } = user;

  return (
    <>
      {isRoleModalOpen === id && (
        <Portal>
          <ChangeRoleModal
            handleModalOpen={setRoleModalOpen}
            currentRole={role}
            userId={id}
            setPage={setPage}
          />
        </Portal>
      )}
      <ErrorMessage error={error} />
      <Card variant="outlined" className={styles.wrapper}>
        <div>
          <Image
            src="/no-photo.png"
            alt={`No Photo Image`}
            width={100}
            height={100}
          />
        </div>
        <div className={styles.contentWrapper}>
          <h3>{username}</h3>
          <p className={styles.text}>Id: {id}</p>
          <p className={styles.text}>Role: {role}</p>
        </div>
        <div className={styles.actionButtonWrapper}>
          {currentUserId === id ? (
            <p>YOU</p>
          ) : (
            <Button
              className={styles.actionButton}
              onClick={() => setRoleModalOpen(id)}
            >
              Change role
            </Button>
          )}
        </div>
      </Card>
    </>
  );
};
