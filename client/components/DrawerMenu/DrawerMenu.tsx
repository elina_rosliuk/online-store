import { Button, SwipeableDrawer } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import styles from './DrawerMenu.module.scss';
import { FC, useState } from 'react';
import { useRouter } from 'next/router';

interface IProps {
  anchor?: 'left' | 'right' | 'top' | 'bottom';
  types: any[];
}

const devicesPath = '/devices';

export const DrawerMenu: FC<IProps> = ({ anchor = 'left', types = [] }) => {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();

  const handlePageChange = (type: string) => {
    setIsOpen(false);
    const path = `${devicesPath}?type=${type}`;
    if (router.asPath !== path) {
      router.push(path);
    }
  };

  return (
    <>
      <Button onClick={() => setIsOpen((prev) => !prev)}>
        <MenuIcon />
      </Button>
      <SwipeableDrawer
        anchor={anchor}
        open={isOpen}
        onClose={() => {}}
        onOpen={() => {}}
      >
        <div className={styles.drawer}>
          <Button onClick={() => setIsOpen((prev) => !prev)}>
            <CloseIcon />
          </Button>
          <ul className={styles.menu}>
            {types &&
              types.map((item) => (
                <li key={item.id} onClick={() => handlePageChange(item.id)}>
                  <Button>{item.title}</Button>
                </li>
              ))}
          </ul>
        </div>
      </SwipeableDrawer>
    </>
  );
};
