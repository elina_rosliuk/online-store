import { FC, SyntheticEvent, useState } from 'react';
import { Button, Container, Rating, Typography } from '@mui/material';
import styles from './Product.module.scss';
import { IDevice, IRate } from '../../types/devices';
import { BreadCrumb } from '../shared/BreadCrumb/BreadCrumb';
import axios from '../../configs/axiosConfig';
import Image from 'next/image';
import { useDispatch } from 'react-redux';
import { fetchBagContent } from '../../store/action-creators/bagActions';
import { ErrorMessage } from '../shared/ErrorMessage/ErrorMessage';

const PREFIX = 'http://localhost:8000/';

interface IProps extends IDevice {
  brand: any;
  type: any;
  bagId: number;
  userId?: number;
  isAuth?: boolean;
  updateProduct: any;
}

export const Product: FC<IProps> = ({
  id,
  title = '',
  description = '',
  image,
  brand,
  type,
  price,
  rates,
  quantity,
  bagId,
  userId,
  isAuth,
  updateProduct,
}) => {
  const [ratingError, setRatingError] = useState('');
  const dispatch = useDispatch();
  const [showAlert, setShowAlert] = useState(false);
  const breadcrumbData = [
    { title: type.title || '', href: `/devices?type=${type.id}` },
    { title: brand.title || '' },
    { title },
  ];

  const getAverageRate = (data: IRate[] = []) => {
    return data.length
      ? data.reduce((prev, current) => prev + current.rate, 0) / data.length
      : 0;
  };

  const addProduct = async () => {
    if (!isAuth) {
      setShowAlert(true);
      setTimeout(() => setShowAlert(false), 5000);
    }

    if (bagId) {
      try {
        const payload = { deviceId: id, bagId };

        await axios.post('bag', payload);
        dispatch(fetchBagContent(bagId));
      } catch (err) {
        console.log(err);
      }
    }
  };

  const setRate = async (
    event: SyntheticEvent<Element, Event>,
    value: number
  ) => {
    setRatingError('');
    const newValue = value || (event.target as HTMLInputElement).value;

    if (isAuth && newValue) {
      try {
        await axios.post(`devices/${id}/rating`, {
          rate: Number(newValue),
          userId,
        });

        updateProduct && updateProduct();
      } catch (err) {
        console.log(err.response);
        setRatingError(err?.response?.data?.message || 'Error');
      }
    } else {
      setRatingError('Log in to set rate');
    }
  };

  return (
    <>
      {showAlert && <ErrorMessage error="Log in to add products to cart" />}
      <Container className={styles.wrapper}>
        <BreadCrumb data={breadcrumbData} />
        <Container className={styles.productWrapper}>
          <div>
            <Image
              src={`${PREFIX}${image}`}
              height={320}
              width={320}
              alt={`Picture`}
            />
          </div>
          <div>
            <h2 className={styles.title}>{title}</h2>
            <div>
              <Rating
                name="product-rating"
                value={getAverageRate(rates)}
                onChange={setRate}
              />
              {ratingError && (
                <Typography component="span" color="red">
                  {ratingError}
                </Typography>
              )}
            </div>
            <div className={styles.buyButtonsWrapper}>
              <p className={styles.priceLabel}>${price}</p>
              {quantity > 0 ? (
                <Button variant="contained" onClick={addProduct}>
                  Add to cart
                </Button>
              ) : (
                <p>Sold out</p>
              )}
            </div>
            <Typography>{description}</Typography>
          </div>
        </Container>
      </Container>
    </>
  );
};
