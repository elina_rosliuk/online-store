import axios from '../../configs/axiosConfig';
import { Dispatch } from 'react';
import {
  ProductsAction,
  ProductsActionTypes,
} from '../../types/productsReducers';
import { ParsedUrlQuery } from 'querystring';

interface IOptions {
  page?: number;
  limit?: number;
  search?: string;
  query?: ParsedUrlQuery;
}

interface IQuery {
  limit?: number;
  search?: string;
  type?: string;
}

export const fetchTypes = () => {
  return async (dispatch: Dispatch<ProductsAction>) => {
    try {
      const response = await axios.get('types');

      dispatch({
        type: ProductsActionTypes.SET_TYPES,
        payload: response.data.rows,
      });
    } catch (err) {
      console.log(err);
      dispatch({
        type: ProductsActionTypes.SET_FETCHING_ERROR,
        payload: 'Error while getting types',
      });
    }
  };
};

export const fetchProducts = (options: IOptions = {}) => {
  return async (dispatch: Dispatch<ProductsAction>) => {
    try {
      const { limit = 5, page = 1, search = '', query } = options;

      const queryParams: IQuery = {};
      if (query) {
        for (const [key, value] of Object.entries(query)) {
          queryParams[key] = value;
        }
      }

      const response = await axios.get(
        `devices?limit=${queryParams.limit || limit}&page=${page}&search=${
          queryParams.search || search
        }${queryParams.type ? `&type=${queryParams.type}` : ''}`
      );

      if (page === 1) {
        dispatch({
          type: ProductsActionTypes.SET_NEW_PRODUCTS,
          payload: {
            products: response.data.rows,
            count: response.data.count,
          },
        });
      } else {
        dispatch({
          type: ProductsActionTypes.SET_PRODUCTS,
          payload: {
            products: response.data.rows,
            count: response.data.count,
          },
        });
      }

      if (query?.type) {
        const response = await axios.get(`types/${query.type}`);

        if (response.data) {
          dispatch({
            type: ProductsActionTypes.SET_CURRENT_TYPE,
            payload: response.data,
          });
        }
      }
    } catch (err) {
      console.log(err);
      dispatch({
        type: ProductsActionTypes.SET_FETCHING_ERROR,
        payload: 'Error while getting products',
      });
      dispatch({
        type: ProductsActionTypes.SET_FETCHING,
        payload: false,
      });
    }
  };
};

export const setFetching = (value: boolean) => {
  return (dispatch: Dispatch<ProductsAction>) => {
    dispatch({
      type: ProductsActionTypes.SET_FETCHING,
      payload: value,
    });
  };
};
