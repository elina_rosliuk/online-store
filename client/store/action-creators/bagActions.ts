import axios from '../../configs/axiosConfig';
import { Dispatch } from 'react';
import {
  BagActionTypes,
  BagsAction,
  IBagContent,
} from '../../types/bagReducers';

const getBagCount = (bag: IBagContent[] = []) => {
  return bag.reduce((prev, curr) => prev + curr.count, 0);
};

export const setBagLoading = (loading: boolean) => {
  return {
    type: BagActionTypes.SET_BAG_LOADING,
    payload: loading,
  };
};

export const fetchBagContent = (id: number) => {
  return async (dispatch: Dispatch<BagsAction>) => {
    try {
      const response = await axios.get('bag/' + id);
      const sortedArr = response.data.devices.sort(
        (item1, item2) => item1.id - item2.id
      );

      dispatch({
        type: BagActionTypes.SET_BAG_TOTAL_COUNT,
        payload: getBagCount(sortedArr),
      });
      dispatch({
        type: BagActionTypes.SET_BAG_CONTENT,
        payload: sortedArr,
      });
    } catch (err) {
      console.log(err);
      dispatch({
        type: BagActionTypes.SET_FETCHING_ERROR,
        payload: 'Error while getting bag content',
      });
    } finally {
      dispatch({
        type: BagActionTypes.SET_BAG_LOADING,
        payload: false,
      });
    }
  };
};

export const clearBagContent = () => ({
  type: BagActionTypes.CLEAR_BAG_CONTENT,
});
