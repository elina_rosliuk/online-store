import { UserActionTypes } from '../../types/userReducers';
import axios from '../../configs/axiosConfig';
import { Dispatch } from 'react';

export const setUser = ({ id, username, role, bagId }) => {
  return {
    type: UserActionTypes.SET_USER,
    payload: {
      id,
      username,
      role,
      bagId,
    },
  };
};

export const setLoading = (isLoading: boolean) => {
  return {
    type: UserActionTypes.SET_LOADING,
    payload: isLoading,
  };
};

export const setBagId = (id: number) => ({
  type: UserActionTypes.SET_BAG_ID,
  payload: id,
});

export const loadUser = () => {
  return async (dispatch: Dispatch<any>) => {
    try {
      dispatch(setLoading(true));
      const res = await axios.get('users/me');
      const { user } = res.data;

      dispatch(
        setUser({
          id: user.id,
          role: user.role,
          username: user.username,
          bagId: user.bagId,
        })
      );
    } catch (err) {
      console.log(err);
    } finally {
      dispatch(setLoading(false));
    }
  };
};
