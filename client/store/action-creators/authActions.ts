import axios from '../../configs/axiosConfig';
import { Dispatch } from 'react';
import { setBagId, setUser } from './userActions';
import { UserActionTypes } from '../../types/userReducers';
import { checkTokenExp } from '../../context/authContext';
import { eraseCookie } from '../../helpers/cookieHelpers';
import { clearBagContent } from './bagActions';

export const checkAuth = () => {
  return async (dispatch: Dispatch<any>) => {
    try {
      const res = await axios.get('token/refresh', { withCredentials: true });

      const { user } = res.data;

      dispatch(
        setUser({
          id: user.id,
          role: user.role,
          username: user.username,
          bagId: user.bagId,
        })
      );
    } catch (err) {
      console.log('err.response.headers', err.response.headers);
      const hasCookie =
        err.response.headers?.cookie?.includes('JWT_REFRESH_TOKEN');
      console.log('hasCookie', hasCookie);
    }
  };
};

export const loginUser = ({ username, password }) => {
  return async (dispatch: Dispatch<any>) => {
    try {
      const response = await axios.post('auth/login', { username, password });
      const {
        data: { user },
      } = response;
      if (user) {
        dispatch(
          setUser({
            id: user.id,
            role: user.role,
            username: user.username,
            bagId: user.bagId,
          })
        );
        dispatch(setBagId(user.bagId));
      } else {
        throw new Error('Authorization failed.');
      }
    } catch (err) {
      console.log(err.response);
      if (err.response.status === 401) {
        dispatch({
          type: UserActionTypes.SET_ERROR,
          payload: err.response?.data?.message,
        });
      } else {
        dispatch({
          type: UserActionTypes.SET_ERROR,
          payload: 'Authorization failed.',
        });
      }
    }
  };
};

export const registerUser = ({ username, password }) => {
  return async (dispatch: Dispatch<any>) => {
    try {
      const response = await axios.post('auth/register', {
        username,
        password,
      });
      const { user } = response.data;

      dispatch(
        setUser({
          id: user.id,
          role: user.role,
          username: user.username,
          bagId: user.bagId,
        })
      );
      dispatch(setBagId(user.bagId));
    } catch (err) {
      if (err.response.status === 401) {
        dispatch({
          type: UserActionTypes.SET_ERROR,
          payload: err.response?.data?.message,
        });
      } else {
        dispatch({
          type: UserActionTypes.SET_ERROR,
          payload: 'Authorization failed.',
        });
      }
      console.log(err);
    }
  };
};

export const clearUser = () => ({
  type: UserActionTypes.LOGOUT,
});

export const logoutUser = () => {
  return async (dispatch: Dispatch<any>) => {
    try {
      dispatch(clearUser());
      dispatch(clearBagContent());
      if (!checkTokenExp()) {
        const response = await axios.post('auth/logout');

        if (response.status >= 400) {
          console.log('error');
        }

        eraseCookie('REFRESH_TOKEN_LIFE_EXP');
      }
    } catch (err) {
      console.log(err);
    }
  };
};
