import axios from '../../configs/axiosConfig';
import { Dispatch } from 'react';
import {
  UserRoleFilterType,
  UsersListAction,
  UsersListActionTypes,
} from '../../types/usersListReducers';

interface IOptions {
  page?: number;
  limit?: number;
  filter?: UserRoleFilterType;
}

export const setLoadingUsersList = (value: boolean) => {
  return (dispatch: Dispatch<UsersListAction>) => {
    dispatch({
      type: UsersListActionTypes.SET_USERS_LIST_LOADING,
      payload: value,
    });
  };
};

export const fetchUsersList = (options: IOptions = {}) => {
  return async (dispatch: Dispatch<UsersListAction>) => {
    try {
      const { limit = 5, page = 1, filter = 'all' } = options;

      const response = await axios.get(
        `users?limit=${limit}&page=${page}&filter=${filter}`
      );
      console.log('response', response);

      if (page === 1) {
        dispatch({
          type: UsersListActionTypes.SET_NEW_USERS_LIST,
          payload: response.data.rows,
        });
        dispatch({
          type: UsersListActionTypes.SET_USERS_LIST_TOTAL_COUNT,
          payload: response.data.count,
        });
      } else {
        dispatch({
          type: UsersListActionTypes.SET_USERS_LIST,
          payload: response.data.rows,
        });
        dispatch({
          type: UsersListActionTypes.SET_USERS_LIST_TOTAL_COUNT,
          payload: response.data.count,
        });
      }
    } catch (err) {
      const errRes = err.response || {};
      console.log(errRes);

      if (errRes.status >= 400 || errRes.status < 500) {
        dispatch({
          type: UsersListActionTypes.SET_USERS_LIST_API_ERROR,
          payload: errRes?.data?.message,
        });
      }

      if (errRes.status >= 500) {
        dispatch({
          type: UsersListActionTypes.SET_USERS_LIST_API_ERROR,
          payload: 'Internal server error',
        });
      }
    } finally {
      dispatch({
        type: UsersListActionTypes.SET_USERS_LIST_LOADING,
        payload: false,
      });
    }
  };
};
