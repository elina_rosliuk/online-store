import {
  UsersListAction,
  UsersListActionTypes,
  UsersListState,
} from '../../types/usersListReducers';

const initialState: UsersListState = {
  users: [],
  error: null,
  loading: false,
  totalCount: 0,
};

export const usersListReducer = (
  state = initialState,
  action: UsersListAction
): UsersListState => {
  switch (action.type) {
    case UsersListActionTypes.SET_NEW_USERS_LIST:
      return {
        ...state,
        users: action.payload,
        error: '',
      };
    case UsersListActionTypes.SET_USERS_LIST:
      return {
        ...state,
        users: [...state.users, ...action.payload],
        error: '',
      };
    case UsersListActionTypes.SET_USERS_LIST_API_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case UsersListActionTypes.SET_USERS_LIST_LOADING:
      return {
        ...state,
        loading: action.payload,
        error: '',
      };
    case UsersListActionTypes.SET_USERS_LIST_TOTAL_COUNT:
      return {
        ...state,
        totalCount: action.payload,
        error: '',
      };
    default:
      return state;
  }
};
