import {
  ProductsAction,
  ProductsState,
  ProductsActionTypes,
} from '../../types/productsReducers';

const initialState: ProductsState = {
  products: [],
  currentProductType: '',
  types: [],
  totalCount: 0,
  isFetching: false,
  error: '',
};

export const productsReducer = (
  state = initialState,
  action: ProductsAction
): ProductsState => {
  switch (action.type) {
    case ProductsActionTypes.SET_CURRENT_TYPE:
      return {
        ...state,
        currentProductType: action.payload,
        error: '',
      };
    case ProductsActionTypes.SET_TYPES:
      return {
        ...state,
        types: action.payload,
        error: '',
      };
    case ProductsActionTypes.SET_PRODUCTS:
      return {
        ...state,
        products: [...state.products, ...action.payload.products],
        totalCount: action.payload.count,
        isFetching: false,
        error: '',
      };
    case ProductsActionTypes.SET_NEW_PRODUCTS:
      return {
        ...state,
        products: action.payload.products,
        totalCount: action.payload.count,
        isFetching: false,
        error: '',
      };
    case ProductsActionTypes.SET_FETCHING:
      return { ...state, isFetching: action.payload, error: '' };
    case ProductsActionTypes.SET_FETCHING_ERROR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
