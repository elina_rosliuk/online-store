import { BagsAction, BagState, BagActionTypes } from '../../types/bagReducers';

const initialState: BagState = {
  bag: [],
  error: null,
  loading: false,
  totalCount: 0,
};

export const bagReducer = (
  state = initialState,
  action: BagsAction
): BagState => {
  switch (action.type) {
    case BagActionTypes.SET_BAG_CONTENT:
      return { ...state, bag: action.payload, error: '' };
    case BagActionTypes.CLEAR_BAG_CONTENT:
      return { bag: [], error: '', loading: false, totalCount: 0 };
    case BagActionTypes.SET_FETCHING_ERROR:
      return { ...state, error: action.payload };
    case BagActionTypes.SET_BAG_LOADING:
      return { ...state, loading: action.payload };
    case BagActionTypes.SET_BAG_TOTAL_COUNT:
      return { ...state, totalCount: action.payload };
    default:
      return state;
  }
};
