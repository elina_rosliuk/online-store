import { HYDRATE } from 'next-redux-wrapper';
import { combineReducers } from 'redux';
import { productsReducer } from './productsReducer';
import { bagReducer } from './bagReducer';
import { userReducer } from './userReducer';
import { usersListReducer } from './usersListReducer';

const rootReducer = combineReducers({
  products: productsReducer,
  bag: bagReducer,
  user: userReducer,
  usersList: usersListReducer,
});

export const reducer = (state, action) => {
  if (action.type === HYDRATE) {
    const userState = state.user;
    const bagState = state.bag;
    console.log('userState', userState);

    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
      user: userState,
      bag: bagState,
    };
    if (state.count) nextState.count = state.count; // preserve count value on client side navigation
    return nextState;
  } else {
    return rootReducer(state, action);
  }
};

export type RootState = ReturnType<typeof rootReducer>;
