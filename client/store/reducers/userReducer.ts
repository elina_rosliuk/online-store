import {
  UserRole,
  UserState,
  UserAction,
  UserActionTypes,
} from '../../types/userReducers';

const initialState: UserState = {
  id: null,
  username: null,
  role: UserRole.CUSTOMER,
  isAuth: false,
  error: null,
  isLoading: false,
  bagId: null,
};

export const userReducer = (
  state = initialState,
  action: UserAction
): UserState => {
  switch (action.type) {
    case UserActionTypes.SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case UserActionTypes.SET_USER:
      return {
        ...state,
        ...action.payload,
        isAuth: true,
        error: '',
      };
    case UserActionTypes.LOGOUT:
      return {
        ...state,
        id: null,
        username: null,
        role: UserRole.CUSTOMER,
        isAuth: false,
        error: null,
      };
    case UserActionTypes.SET_BAG_ID:
      return { ...state, bagId: action.payload, error: '' };
    case UserActionTypes.SET_ERROR:
      return { ...state, isLoading: false, error: action.payload };
    default:
      return state;
  }
};
